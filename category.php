<?php

/* Category Archive */

get_header();

// Get Category Data
$cat = get_query_var('cat');
$current_cat = get_category($cat);

$parent_cat_id = $current_cat->category_parent;
$catID = $current_cat->term_id;

$cat_selector = 'category_' . $catID;

if($parent_cat_id != 0) {
	$parent_cat_arr = get_category($parent_cat_id);

	$cat_slug = $parent_cat_arr->slug;
	$cat_name = $parent_cat_arr->cat_name;
	$child_name = $current_cat->cat_name;
	$child_slug = $current_cat->slug;
} else {
	$cat_slug = $current_cat->slug;
	$cat_name = $current_cat->cat_name;
}

$exclude = array(7992);
?>

<div class="container-fluid">
  <div class="top-section">
    <div class="col-sm-9 no-right-padding">


			<?php include(locate_template(Healthbeat_Helper::$parts . 'taxonomies/category-header.php')); ?>


      <div class="posts">
        <?php
          $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

          $args = array(
            'posts_per_page' => 12,
            'post_type' => array('post', 'infographics', 'videos', 'patient-stories'),
            'paged' => $paged,
            'post__not_in' => $exclude,
            'tax_query' => array(
              'relation' => 'AND',
              array (
                'taxonomy' => 'expert_blog',
                'field' => 'id',
                'terms' => array(19,20,21,22,23,24,25,26,97),
                'operator' => 'NOT IN',
              ),
              array(
                'relation' => 'OR',
                array(
                  'taxonomy' => 'category',
                  'field'    => 'slug',
                  'terms'    => array( $current_cat->slug ),
                ),
                array(
                  'taxonomy' => 'post_tag',
                  'field'    => 'slug',
                  'terms'    => array( "tag-" . $current_cat->slug ),
                ),
                array(
                  'taxonomy' => 'secondary_tags',
                  'field'    => 'slug',
                  'terms'    => array( "tag-" . $current_cat->slug ),
                ),
              )
            ),

          );

          $temp = $wp_query;
          $wp_query = null;
          $wp_query = new WP_Query($args);

          if($wp_query->have_posts()) {
            while($wp_query->have_posts()) {
              $wp_query->the_post();
              include(locate_template(Healthbeat_Helper::$parts . 'components/post-list.php'));
            }

            healthbeat_pagination();
          }

          $wp_query = null;
          $wp_query = $temp;

          wp_reset_postdata();
        ?>
      </div>
    </div>

    <div class="col-sm-3">
      <?php get_sidebar('main-sidebar'); ?>
    </div>
  </div>

  <div class="clear clearfix"></div>
</div>
<?php get_footer(); ?>
