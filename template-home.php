<?php

/**
* Template Name: Home
*
* @package	healthbeat
*/

get_header(); ?>

<div id="<?php echo $post->post_name; ?>">
	<div id="featured-posts">
		<?php
			$exclue = array();

			if(have_rows('featured_posts')) {
				while(have_rows('featured_posts')) {
					the_row();

					$layout = get_row_layout();

					if(get_sub_field('active_layout'))
						include(locate_template(Healthbeat_Helper::$parts . 'home/featured-' . $layout . '.php'));
				}
			}
		?>
	</div>

	<div class="container-fluid">
		<div class="top-section">
			<div class="col-sm-9 no-left-padding">
				<?php include(locate_template(Healthbeat_Helper::$parts . 'home/section-posts.php')); ?>
			</div>

			<div class="col-sm-3">
				<?php get_sidebar('main-sidebar'); ?>
			</div>
		</div>

		<div class="clear clearfix"></div>
	</div><!-- .container -->

	<?php get_template_part(Healthbeat_Helper::$parts . 'home/section-editors', 'picks'); ?>

	<?php get_template_part(Healthbeat_Helper::$parts . 'home/section', 'subscribe'); ?>

	<?php get_template_part(Healthbeat_Helper::$parts . 'home/section', 'photos'); ?>

	<?php get_template_part(Healthbeat_Helper::$parts . 'home/section', 'videos'); ?>

	<?php get_template_part(Healthbeat_Helper::$parts . 'home/section', 'ctas'); ?>

	<?php get_template_part(Healthbeat_Helper::$parts . 'home/section', 'specialseries'); ?>

	<?php get_template_part(Healthbeat_Helper::$parts . 'home/section', 'infographics'); ?>

	<?php get_template_part(Healthbeat_Helper::$parts . 'components/block', 'popularposts'); ?>
</div>

<?php
get_footer();
