<?php
/**
 * The template for displaying all pages.
 * Required by WP
 *
 * @package healthbeat
 */

get_header(); ?>

<div id="<?php echo $post->post_name; ?>">
	<div class="container-fluid page">
		<div class="col-sm-9 no-right-padding">

			<?php get_template_part(Healthbeat_Helper::$parts . 'components/page', 'header'); ?>

	    <div class="clear clearfix"></div>
		</div>
		<div class="col-sm-3">
			<?php get_sidebar('main-sidebar'); ?>
		</div>
	</div><!-- .container -->
</div>
<?php
get_footer();
