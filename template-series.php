<?php
/**
 * Template Name: Special Series Landing
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package healthbeat
 */

get_header();

?>

<div id="special-series" class="container-fluid page">
	<div class="col-sm-9 no-right-padding">

		<?php get_template_part(Healthbeat_Helper::$parts . 'components/page', 'header'); ?>

    <div class="clear clearfix"></div>

    <div class="posts">
      <?php

        // Get All Expert Blogs
        $args = array(
          'hide_empty' => true,
          'orderby' => 'term_order'
        );

        $count = 0;

        // Get the series terms
        $series_arr = get_terms('special-series', $args);

        // Loop the sorted array and display the series
        foreach($series_arr as $series) {
          // Increment the count
          $count++;

          // Create the ACF series ID to extract custom meta-data
          $series_selector = "special-series_" . $series->term_id;
          // Series landing image
          $series_image = get_field('series_landing_image', $series_selector);
          // Series landing excerpt
          $series_excerpt = get_field('series_excerpt', $series_selector);

          $series_url = '/series/' . $series->slug . '/';

          include(locate_template(Healthbeat_Helper::$parts . 'components/series-list.php'));
        }

      ?>
    </div>
	</div>
	<div class="col-sm-3">
		<?php get_sidebar('main-sidebar'); ?>
	</div>
</div><!-- .container -->

<?php
get_footer();
