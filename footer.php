<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @package healthbeat
 */

?>

		<footer>

			<div class="footer-links">
				<div class="container-fluid">
					<div class="col-md-12">
						<div class="logo">
							<a href="/"><img src="<?php echo get_template_directory_uri(); ?>/img/footer-logo.png" alt="Spectrum Health - Health Beat" title="spectrum-health-health-beat" /></a>
						</div>
					</div>
					<div class="col-md-8 no-padding">
						<div class="col-md-3 col-xs-6">
							<?php dynamic_sidebar('footer-column-one'); ?>
						</div>
						<div class="col-md-4  col-xs-6 double-left-padding">
							<?php dynamic_sidebar('footer-column-two'); ?>
						</div>
						<div class="col-md-5 col-xs-12 no-left-padding contact-sh">
							<?php //dynamic_sidebar('footer-column-three'); ?>

							<div class="footer-column">
								<h2 class="column-title"><?php _e('Contact Spectrum Health', 'healthbeat'); ?></h2>
								<hr />

								<?php get_template_part(Healthbeat_Helper::$parts . 'components/footer-contact', 'links'); ?>
							</div>
						</div>
					</div>
					<div class="col-md-4 no-padding">

						<div class="col-md-12 no-padding">
							<div class="subscribe-box">
								<div class="col-sm-6 no-left-padding">
									<h2><?php _e('Free e-News', 'healthbeat'); ?></h2>
									<p>
										<?php _e('Our best stories delivered to your inbox!', 'healthbeat'); ?>
									</p>
								</div>
								<div class="col-sm-6 no-right-padding">
									<a href="<?php echo Healthbeat_Helper::$subscribe_url; ?>" class="btn btn-default btn-green-gradient large"><?php _e('Subscribe', 'healthbeat'); ?></a>
								</div>
							</div>
						</div>

						<div class="col-xs-6 no-left-padding">
							<?php dynamic_sidebar('footer-column-four'); ?>
						</div>
						<div class="col-xs-6 no-right-padding">
							<?php dynamic_sidebar('footer-column-five'); ?>
						</div>
					</div>
				</div>
			</div>

			<div class="copyright text-right">
				<div class="container-fluid">
					<ul>
						<li><a href="http://www.spectrumhealth.org/login" target="_blank"><?php _e('Log In', 'healthbeat'); ?></a></li>
						<li><a href="http://www.spectrumhealth.org/visitors-and-families/nondiscrimination-notice" target="_blank"><?php _e('Nondiscrimination Notice', 'healthbeat'); ?></a></li>
						<li><a href="http://www.spectrumhealth.org/policies/website-privacy-policy" target="_blank"><?php _e('Website Privacy Policy', 'healthbeat'); ?></a></li>
						<li><a href="http://www.spectrumhealth.org/policies/patient-privacy" target="_blank"><?php _e('Patient Privacy', 'healthbeat'); ?></a></li>
						<li><a href="http://www.spectrumhealth.org/policies/terms-of-use" target="_blank"><?php _e('Terms of Use', 'healthbeat'); ?></a></li>
						<li><a href="http://www.spectrumhealth.org/the-joint-commission" target="_blank"><?php _e('Joint Commission', 'healthbeat'); ?></a></li>
					</ul>
					<div class="clear clearfix"></div>
					<div class="copyright-text">
						100 Michigan St. NE, Grand Rapids, MI 49503<br>
						Copyright &copy; 2017 Spectrum Health. All Rights Reserved.<br> <br>
						The content on this website is provided for informational purposes only and is not intended as medical advice. <br>Please consult a physician regarding your specific medical condition, diagnosis and/or treatment. <br>The translation service utilizes an automated translator. No automated translator is perfect. The official text is the English version. <br>Any discrepancies or differences created in the translation have no legal effect.
					</div>
				</div>
			</div>
		</footer>
	</div> <!-- /#content -->
</div> <!-- /#main-canvas -->

<?php wp_footer(); ?>
</body>
</html>
