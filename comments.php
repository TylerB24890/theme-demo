<?php
/**
 * The template for displaying comments.
 *
 * The area of the page that contains both current comments
 * and the comment form.
 *
 * @package healthbeat
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>

<div id="comments" class="comments-area">

	<?php if ( have_comments() ) : ?>
		<h2 class="comments-title">
			<?php _e('Comments', 'healthbeat'); ?> <span>(<?php echo get_comments_number(); ?>)</span>
		</h2>

		<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // are there comments to navigate through ?>
		<nav id="comment-nav-above" class="comment-navigation" role="navigation">
			<h1 class="screen-reader-text"><?php _e( 'Comment navigation', 'healthbeat' ); ?></h1>
			<div class="nav-previous"><?php previous_comments_link( __( '&larr; Older Comments', 'healthbeat' ) ); ?></div>
			<div class="nav-next"><?php next_comments_link( __( 'Newer Comments &rarr;', 'healthbeat' ) ); ?></div>
		</nav><!-- #comment-nav-above -->
		<?php endif; // check for comment navigation ?>

		<ul class="comment-list">
			<?php
				wp_list_comments( array(
					'style'      => 'ul',
					'short_ping' => true,
					'avatar_size' => 0,
					'reply_text' => "Reply",
				) );

				echo comment_reply_link();
			?>
		</ul><!-- .comment-list -->

		<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // are there comments to navigate through ?>
		<nav id="comment-nav-below" class="comment-navigation" role="navigation">
			<h1 class="screen-reader-text"><?php _e( 'Comment navigation', 'healthbeat' ); ?></h1>
			<div class="nav-previous"><?php previous_comments_link( __( '&larr; Older Comments', 'healthbeat' ) ); ?></div>
			<div class="nav-next"><?php next_comments_link( __( 'Newer Comments &rarr;', 'healthbeat' ) ); ?></div>
		</nav><!-- #comment-nav-below -->
		<?php endif; // check for comment navigation ?>

	<?php endif; // have_comments() ?>

	<?php
		// If comments are closed and there are comments, let's leave a little note, shall we?
		if ( ! comments_open() && '0' != get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) :
	?>
		<p class="no-comments"><?php _e( 'Comments are closed.', 'healthbeat' ); ?></p>
	<?php endif; ?>


	<?php

		if(!is_user_logged_in()) :
			$class = "col-sm-6 col-sm-push-6 no-padding";
		else :
			$class = "col-sm-12 no-padding";
		endif;


		$f_args = array(
			'id_form'           => 'commentform',
			'id_submit'         => 'submit',
			'title_reply'       => __( 'Comment on this story' ),
			'title_reply_to'    => __( 'Leave a Reply to %s' ),
			'cancel_reply_link' => __( 'Cancel Reply' ),
			'label_submit'      => __( 'Post Comment' ),
			'class_submit'			=> 'btn btn-default btn-green-gradient large',
			'comment_field' =>  '<div class="comment-fields"><div class="' . $class . '"><div class="form-group"><label for="comment">' . _x( 'Comment', 'noun' ) .
			 '</label><textarea id="comment" name="comment" class="form-control" aria-required="true">' .
			 '</textarea></div></div>' . (is_user_logged_in() ? '</div>' : ''),

			'must_log_in' => '<p class="must-log-in">' .
				sprintf(
					__( 'You must be <a href="%s">logged in</a> to post a comment.' ),
					wp_login_url( apply_filters( 'the_permalink', get_permalink() ) )
				) . '</p>',

			'logged_in_as' => '<p class="logged-in-as">' .
				sprintf(
					__( 'Logged in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Log out?</a>' ),
					admin_url( 'profile.php' ),
					$user_identity,
					wp_logout_url( apply_filters( 'the_permalink', get_permalink( ) ) )
				) . '</p>',

			'comment_notes_before' => '<p class="comment-notes">' .
				__( 'Your email address will not be published. Required fields are marked <sup>*</sup>' ) .
			'</p>',

			'comment_notes_after' => '',

			'fields' => apply_filters( 'comment_form_default_fields', array(

			'author' =>
			'<div class="col-sm-6 col-sm-pull-6 no-left-padding"><div class="form-group">' .
			'<label for="author">' . __( 'Name', 'healthbeat' ) . '</label> ' .
			( $req ? '<span class="required">*</span>' : '' ) .
			'<input id="author" name="author" type="text" class="form-control" value="' . esc_attr( $commenter['comment_author'] ) .
			'" size="30"/></div>',

			'email' =>
				'<div class="form-group"><label for="email">' . __( 'Email', 'healthbeat' ) . '</label> ' .
				( $req ? '<span class="required">*</span>' : '' ) .
				'<input id="email" name="email" type="text" class="form-control" value="' . esc_attr(  $commenter['comment_author_email'] ) .
				'" size="30" /></div>',

			'url' =>
				'<div class="form-group"><label for="url">' .
				__( 'Website', 'healthbeat' ) . '</label>' .
				  '<input id="url" name="url" type="text" class="form-control" value="' . esc_attr( $commenter['comment_author_url'] ) .
				  '" size="30" /></div></div></div>'
				)
			),
		);

	?>


	<?php comment_form($f_args); ?>

</div><!-- #comments -->
