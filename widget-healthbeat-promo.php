<?php

/**
 * Custom ACF Widget
 *
 * HealthBeat Promos
 */

// Get current page/post data
global $post;
$post_data = Healthbeat_Posts::get_post_data($post->ID);

// Get widget data
$widget_title = get_field('header', $acfw);
$widget_image = get_field('image', $acfw);
$widget_link = get_field('link', $acfw);
$widget_cta = get_field('cta', $acfw);
$widget_categories = get_field('category', $acfw);
$widget_content_types = get_field('display_on_content_types', $acfw);
$start_date = get_field('start_date', $acfw);
$end_date = get_field('end_date', $acfw);

$widget_special_series = get_field('special_series', $acfw);
$widget_brand_series = get_field('brand_series', $acfw);
$widget_hospitals = get_field('hospital', $acfw);

// Link the promo?
$linked = false;
// Display the promo?
$display = false;

$tax_id = (is_tax() ? get_queried_object_id() : 0);

// Check promo content types
if($widget_content_types && !empty($widget_content_types)) {
  if(in_array('global', $widget_content_types)) {
    $display = true;
  } else {
    if(in_array('homepage', $widget_content_types)) {
      if(is_front_page() || is_home()) {
        $display = true;
      }
    }

    if(in_array('post', $widget_content_types)) {
      if(is_single()) {

        if(in_array('patient-stories', $widget_content_types)) {
          if($post_data['patient_story']) {
            if(!empty($widget_categories)) {
              $display = Healthbeat_Widgets::check_promo_category($widget_categories, $post_data['category']['id']);
            } else {
              $display = true;
            }
          }
        } elseif(in_array('special-series', $widget_content_types)) {
          if($post_data['special_series']) {

            if(!empty($widget_categories)) {
              $display = Healthbeat_Widgets::check_promo_category($widget_categories, $post_data['category']['id']);
            } else {
              $display = true;
            }
          }
        } elseif(in_array('photo-galleries', $widget_content_types)) {
           if($post_data['photo_gallery']) {

             if(!empty($widget_categories)) {
               $display = Healthbeat_Widgets::check_promo_category($widget_categories, $post_data['category']['id']);
             } else {
               $display = true;
             }
           }
        } elseif(in_array('brand-series', $widget_content_types)) {
           if($post_data['brand_series']) {

             if(!empty($widget_categories)) {
               $display = Healthbeat_Widgets::check_promo_category($widget_categories, $post_data['category']['id']);
             } else {
               $display = true;
             }
           }
        } elseif(in_array('infographics', $widget_content_types)) {
           if(is_single() && $post_data['post_type'] === 'infographics') {

             if(!empty($widget_categories)) {
               $display = Healthbeat_Widgets::check_promo_category($widget_categories, $post_data['category']['id']);
             } else {
               $display = true;
             }
           }
        } else {
          if(is_array($widget_categories)) {
            $display = Healthbeat_Widgets::check_promo_category($widget_categories, $post_data['category']['id']);
          } else {
            $display = true;
          }
        }
      }
    }

    if(in_array('infographics', $widget_content_types)) {
      if(is_single() && $post_data['post_type'] === 'infographics') {
        $display = Healthbeat_Widgets::check_promo_category($widget_categories, $post_data['category']['id']);
      } elseif(is_page('infographics')) {
        $display = true;
      }
    }

    if(in_array('patient-stories', $widget_content_types)) {
      if($post_data['patient_story']) {
        if(!empty($widget_categories)) {
          $display = Healthbeat_Widgets::check_promo_category($widget_categories, $post_data['category']['id']);
        } else {
          $display = true;
        }
      }
    }

    if(in_array('photo-galleries', $widget_content_types)) {
      if($post_data['photo_gallery']) {
        $display = Healthbeat_Widgets::check_promo_category($widget_categories, $post_data['category']['id']);
      }
    }

    if(in_array('brand-series', $widget_content_types)) {
      if($post_data['brand_series']) {
        $display = Healthbeat_Widgets::check_promo_category($widget_categories, $post_data['category']['id']);
      }
    }

    if(in_array('special-series', $widget_content_types)) {
      if($post_data['special_series']) {

        if(!empty($widget_categories)) {
          $display = Healthbeat_Widgets::check_promo_category($widget_categories, $post_data['category']['id']);
        } else {
          $display = true;
        }
      }
    }
  }
}

// Check promo categories
if($widget_categories && !empty($widget_categories)) {
  if($post_data['category'] && is_numeric($post_data['category']) && in_array($post_data['category'], $widget_categories)) {
    $display = true;
  }
}

// Check Promo Special Series
if($widget_special_series && !empty($widget_special_series)) {
  if(is_single()) {
    $special_series = wp_get_post_terms($post->ID, 'special-series');

    if($special_series && is_array($special_series)) {
      foreach($special_series as $series) {
        if(in_array($series->term_id, $widget_special_series)) {
          $display = true;
        }
      }
    }
  } elseif($tax_id !== 0 && in_array($tax_id, $widget_special_series)) {
    $display = true;
  }
}

// Check Promo Brand Series
if($widget_brand_series && !empty($widget_brand_series)) {
  if(is_single()) {
    $brand_series = wp_get_post_terms($post->ID, 'brand-series');

    if($brand_series && is_array($brand_series)) {
      foreach($brand_series as $series) {
        if(in_array($series->term_id, $widget_brand_series)) {
          $display = true;
        }
      }
    }
  } elseif($tax_id !== 0 && in_array($tax_id, $widget_brand_series)) {
    $display = true;
  }
}

// Check Promo Hospital
if($widget_hospitals && !empty($widget_hospitals)) {
  if(is_single()) {
    $hospitals = wp_get_post_terms($post->ID, 'hospitals');

    if($hospitals && is_array($hospitals)) {
      foreach($hospitals as $hospital) {
        if(in_array($hospital->term_id, $widget_hospitals)) {
          $display = true;
        } else {
          $display = false;
        }
      }
    }
  } elseif($tax_id !== 0 && in_array($tax_id, $widget_hospitals)) {
    $display = true;
  }
}

// Check the promo start date
if($start_date && strlen($start_date) > 1) {
  if(strtotime($start_date) >= time()) {
    $display = false;
  }
}
// Check the promo end date
if(strtotime($end_date) <= time()) {
  $display = false;
}

// Check if the widget is linked
if($widget_link && strlen($widget_link) > 1) {
  $linked = true;
}

?>

<?php if($display) : ?>
  <div class="widget-block">
    <h2 class="widget-title"><?php echo $widget_title; ?></h2>

    <div class="widget-image">
      <?php echo($linked ? '<a href="' . $widget_link . '">' : ''); ?>
        <img src="<?php echo $widget_image; ?>" alt="<?php echo $widget_title; ?>" title="<?php echo sanitize_title_with_dashes($widget_title); ?>"/>
      <?php echo($linked ? '</a>' : ''); ?>
    </div>

    <?php if($widget_cta && strlen($widget_cta) > 0) : ?>
      <div class="widget-cta">
        <?php echo($linked ? '<a class="btn btn-default btn-green-gradient large" href="' . $widget_link . '">' : ''); ?>
          <?php echo $widget_cta; ?>
        <?php echo($linked ? '</a>' : ''); ?>
      </div>
    <?php endif; ?>
  </div>
<?php endif; ?>
