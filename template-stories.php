<?php
/**
 * Template Name: All Stories
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package healthbeat
 */

get_header();

?>

<div id="archive" class="container-fluid">
	<div class="col-sm-9 no-right-padding">

		<div class="col-md-12">
			<div class="archive-header">
				<h2><?php the_title(); ?></h2>
			</div>
		</div>

			<?php

      $args = array(
        'paged' => $paged,
        'post_type' => array('post', 'infographics', 'videos', 'patient-stories'),
        'posts_per_page' => 20,
      );

      $temp = $wp_query;
      $wp_query = null;
      $wp_query = new WP_Query($args);

      if($wp_query->have_posts()) {
        while($wp_query->have_posts()) {
          $wp_query->the_post();
          $exclude[] = $post->ID;
          include(locate_template(Healthbeat_Helper::$parts . 'components/post-list.php'));
        }

        healthbeat_pagination();
      }

      $wp_query = null;
      $wp_query = $temp;

      wp_reset_query();

			?>

	</div>
	<div class="col-sm-3">
		<?php get_sidebar('main-sidebar'); ?>
	</div>
</div><!-- .container -->

<?php
get_footer();
