<?php
/**
 * Template Name: Regional Hospitals
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package healthbeat
 */

get_header();

?>

<div id="hospitals" class="container-fluid page">
	<div class="col-sm-9 no-right-padding">

    <?php
      if(have_posts()) :
        while(have_posts()) :
          the_post();
    ?>
      		<div class="col-md-12">
      			<div class="page-header-container">
      				<h1><?php _e('Regional Hospitals', 'healthbeat'); ?></h1>
      			</div>
      		</div>

          <div class="clear clearfix"></div>

					<?php

					// Get All Hospitals
					$args = array(
						'taxonomy' => 'hospitals',
						'hide_empty' => false,
					);

					$count = 0;

					$hospitals = get_terms($args);

					foreach($hospitals as $hospital) :
					?>
						<div id="<?php echo $hospital->slug; ?>" class="col-md-6">
							<div class="hospital-block block">
								<div class="block-img">
									<img src="<?php the_field('hospital_image', 'hospitals_' . $hospital->term_id); ?>" alt="<?php echo $hospital->name; ?>" title="<?php echo $hospital->slug; ?>">
								</div>
								<div class="block-content">
									<a class="block-title" href="<?php echo '/hospital/' . $hospital->slug; ?>"><?php echo $hospital->name; ?></a>

									<a class="btn-outline" href="<?php echo '/hospital/' . $hospital->slug; ?>"><?php _e('View stories', 'healthbeat'); ?></a>
								</div>
							</div>
						</div>
				<?php endforeach; ?>

    <?php endwhile; endif; ?>
	</div>
	<div class="col-sm-3">
		<?php get_sidebar('main-sidebar'); ?>
	</div>
</div><!-- .container -->

<?php
get_footer();
