<?php

/* Category Archive */

get_header();

?>

<div class="container-fluid">
  <div class="top-section">
    <div class="col-sm-9">
			<?php
			while(have_posts()) :
				the_post();
				$post_data = Healthbeat_Posts::get_post_data($post->ID);
			?>
				<div class="post <?php echo $post_data['category']['slug']; ?>">

					<?php Healthbeat_Posts::render_post($post_data); ?>

          <div class="clear clearfix"></div>

          <?php Healthbeat_Posts::render_post_ctas($post); ?>

          <div class="col-lg-9 no-padding">
            <div class="single-share">
              <div class="share-box">
                <?php echo do_shortcode('[hb_share_list list_title="Share It:" post_id="' . $post->ID . '"]'); ?>
              </div>
              <div class="subscribe-box">
                <a href="<?php echo Healthbeat_Helper::$subscribe_url; ?>" class="btn btn-default btn-green-gradient large"><?php _e('Subscribe', 'healthbeat'); ?></a>
              </div>
            </div>
          </div>

				</div>

        <?php comments_template(); ?>
			<?php endwhile; ?>
    </div>

    <div class="col-sm-3">
      <?php get_sidebar('main-sidebar'); ?>
    </div>
  </div>

  <div class="clear clearfix"></div>
</div>
<?php get_footer(); ?>
