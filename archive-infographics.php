<?php
/**
 * Infographics Archive
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package healthbeat
 */

get_header();

$archive = Healthbeat_Helper::get_archive_data();

?>

<div id="infographics" class="container-fluid page">
	<div class="col-sm-9 no-right-padding">

    <div class="col-md-12">
      <div class="page-header-container">

				<h1><?php echo str_replace('Archives: ', '', get_the_archive_title()); ?></h1>

        <p>
          <?php echo get_option('infographics_header'); ?>
        </p>
      </div>
    </div>

		<div class="clear clearfix"></div>

		<div class="posts infographic-masonry">
			<div class="col-md-4 grid-sizer"></div>

			<?php

			$exclude = array();

			if(have_posts()) {
				while(have_posts()) {
					the_post();
					$exclude[] = $post->ID;
					include(locate_template(Healthbeat_Helper::$parts . 'components/infographic-block.php'));
				}

				echo do_shortcode('[ajax_load_more container_type="div" post_type="infographics" posts_per_page="9" scroll="false" transition="fade" pause="true" exclude="' . implode(',', $exclude) . '" transition_container="false" images_loaded="true" button_label="' . __('Load More', 'healthbeat') . '" button_loading_label="' . __('Loading Infographics...', 'healthbeat') . '" transition_speed="0"]');
			}

			?>
		</div>

	</div>
	<div class="col-sm-3">
		<?php get_sidebar('main-sidebar'); ?>
	</div>
</div><!-- .container -->

<?php
get_footer();
