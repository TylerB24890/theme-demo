<?php
/**
 * Template Name: Brand Series Landing
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package healthbeat
 */

get_header();

?>

<div id="brand-series-landing" class="container-fluid page">
	<div class="col-sm-9 no-right-padding">

		<?php get_template_part(Healthbeat_Helper::$parts . 'components/page', 'header'); ?>

    <div class="clear clearfix"></div>

    <div class="posts">
      <?php

        // Get All Expert Blogs
        $args = array(
          'hide_empty' => false,
          'orderby' => 'term_order'
        );

        // Get the series terms
        $series_arr = get_terms('brand-series', $args);

        // Loop the sorted array and display the series
        foreach($series_arr as $series) :

          // Create the ACF series ID to extract custom meta-data
          $series_selector = "brand-series_" . $series->term_id;
          // Series landing image
          $series_image = get_field('series_landing_banner', $series_selector);
          // Series landing excerpt
          $series_content = get_field('banner_content', $series_selector);
          // Align content
          $align = (get_field('align_content', $series_selector) === 'right' ? 'pull-right no-left-padding' : 'pull-left');
      ?>

            <div class="col-md-12">
              <div class="series-banner" style="background: url('<?php echo $series_image; ?>') no-repeat center center;">
                <div class="col-md-5 banner-content <?php echo $align; ?>">
                  <h2><?php echo $series->name; ?></h2>

                  <p>
                    <?php echo $series_content; ?>
                  </p>

                  <a href="<?php echo get_term_link($series->term_id); ?>" class="btn btn-default btn-green-gradient large"><?php the_field('banner_link_text', $series_selector); ?></a>
                </div>
              </div>
            </div>

      <?php endforeach; ?>
    </div>
	</div>
	<div class="col-sm-3">
		<?php get_sidebar('main-sidebar'); ?>
	</div>
</div><!-- .container -->

<?php
get_footer();
