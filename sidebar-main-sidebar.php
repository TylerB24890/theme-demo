

<div id="sidebar" class="main-sidebar">
   <div class="widget-area">
      <div class="sidebar-cta">
         <a class="btn btn-primary btn-blue with-arrow" href="<?php echo Healthbeat_Helper::$subscribe_url; ?>">
            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
               viewBox="0 0 21 21" style="enable-background:new 0 0 21 21;" xml:space="preserve">
               <style type="text/css">
                  .st0{fill:none;stroke:#FFFFFF;stroke-width:0.9649;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;}
                  .st1{fill:none;stroke:#FFFFFF;stroke-width:0.9649;stroke-linejoin:round;stroke-miterlimit:10;}
                  .st2{fill:none;stroke:#FFFFFF;stroke-width:0.9649;stroke-miterlimit:10;}
               </style>
               <polyline class="st0" points="5.9,12.5 5.9,7.5 15.1,7.5 15.1,12.4 "/>
               <line class="st0" x1="7.9" y1="9.8" x2="13.1" y2="9.8"/>
               <line class="st0" x1="7.9" y1="11.1" x2="13.1" y2="11.1"/>
               <line class="st0" x1="7.9" y1="12.5" x2="13.1" y2="12.5"/>
               <g>
                  <polygon class="st1" points="20.2,8.5 20.2,20.6 0.6,20.6 0.6,8.5 10.4,16 	"/>
                  <line class="st2" x1="0.6" y1="20.6" x2="7.5" y2="13.7"/>
                  <line class="st2" x1="20.2" y1="20.6" x2="13.4" y2="13.7"/>
                  <polyline class="st1" points="0.6,8.5 10.4,1.2 20.2,8.5 	"/>
               </g>
            </svg>
            <?php _e('Subscribe', 'healthbeat'); ?>
         </a>
         <a class="btn btn-primary btn-blue with-arrow" href="https://healthbeat.spectrumhealth.org/contact-us/share-your-story/">
            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
               viewBox="0 0 21 18.2" style="enable-background:new 0 0 21 18.2;" xml:space="preserve">
               <style type="text/css">
                  .st4{fill:none;stroke:#FFFFFF;stroke-width:0.8916;stroke-miterlimit:10;}
                  .st5{fill:none;stroke:#FFFFFF;stroke-width:0.8916;stroke-linecap:round;stroke-miterlimit:10;}
               </style>
               <path class="st4" d="M14.3,12.1v5.1c0,0.4-0.3,0.7-0.7,0.7H2.8c-0.4,0-0.7-0.3-0.7-0.7V1.3c0-0.4,0.3-0.7,0.7-0.7h10.8
                  c0.4,0,0.7,0.3,0.7,0.7V7"/>
               <path class="st0" d="M19.8,5.6l-1.5-1.5c-0.3-0.3-0.8-0.3-1.1,0l-8.4,8.4c-0.3,0.3-0.3,0.8,0,1.1l1.5,1.5c0.3,0.3,0.8,0.3,1.1,0
                  l8.4-8.4C20.1,6.3,20.1,5.9,19.8,5.6z"/>
               <path class="st4" d="M7.8,16.2l2.8-0.8c0.1,0,0.1-0.1,0-0.2l-2-2c-0.1-0.1-0.2,0-0.2,0.1L7.8,16.2C7.7,16.2,7.8,16.2,7.8,16.2z"/>
               <line class="st5" x1="4.4" y1="4.7" x2="11.9" y2="4.7"/>
               <line class="st5" x1="4.4" y1="8.4" x2="10.8" y2="8.4"/>
            </svg>
            <?php _e('Share your Story', 'healthbeat'); ?>
         </a>
      </div>
   </div>
   <div class="widget-area">
      <h2 class="widget-title"><?php _e('Resources for You', 'healthbeat'); ?></h2>
      <ul id="menu-sidebar-resources">
         <li class="menu-item">
            <a href="https://findadoctor.spectrumhealth.org/#/" target="_blank" class="btn-outline">
               <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                  viewBox="0 0 19 17" style="enable-background:new 0 0 19 17;" xml:space="preserve">
                  <style type="text/css">
                     .stdoc{fill:#3D78B8;}
                  </style>
                  <path class="stdoc" d="M8.5,9.9c-1.3-0.6-2.2-2.1-2.3-4c0.7,0.1,1.6-0.6,2-1.7c0,0,0,0,0.1,0.1c1,1.2,3.3,2.1,4.7,2.4
                     c-0.3,1.5-1.1,2.7-2.2,3.2c0.1,0.2,0.4,0.5,1,0.7c0.8,0.2,1.3-0.2,1.3-0.2c2-2.5,1.1-5.5,1.1-5.5l0,0c-0.2-1-0.7-2.2-1.5-3.1
                     c-1.5-1.7-4-1.9-4.9-0.9C7.6,1,7.5,1.1,7.4,1.2C6.6,1,5.7,1.9,5.3,3.1c0,0.1-0.1,0.2-0.1,0.3C5.1,4.3,4.6,7,5.4,9.5
                     C6,11,7.3,10.7,8.5,9.9z M11.8,11.1l-2.2,2.3l-2.3-2.3C4.9,11.5,2.4,13,2.4,13l0.9,3.7h1.3l-0.9-3.1l1.5-0.7L9,15.8v0.8h1v-0.8
                     l3.8-2.9l1.5,0.6l-1,3.1h1.4l0.8-3.6C15.5,11.9,11.8,11.1,11.8,11.1z M12,15.6h2.1v-0.5H12V15.6z"/>
               </svg>
               <?php _e('Find a Doctor', 'healthbeat'); ?>
            </a>
         </li>
         <li class="menu-item">
            <a href="https://give.spectrumhealth.org/donate" target="_blank" class="btn-outline">
               <svg version="1.1" id="Layer_2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                  viewBox="0 0 15.6 17" style="enable-background:new 0 0 15.6 17;" xml:space="preserve">
                  <style type="text/css">
                     .st3{fill:none;stroke:#3D78B8;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;}
                  </style>
                  <rect x="1.8" y="4.6" class="st3" width="12.2" height="3.8"/>
                  <polyline class="st3" points="2.5,8.4 2.5,16 13.3,16 13.3,8.4 "/>
                  <line class="st3" x1="9.5" y1="16" x2="9.5" y2="4.8"/>
                  <line class="st3" x1="6.4" y1="16" x2="6.4" y2="5"/>
                  <path class="st3" d="M7.9,4.6L6.2,1.8c0,0-0.5-0.8-1.6-0.8S2.9,1.8,2.9,2.7c0,1.1,1.6,1.9,1.6,1.9"/>
                  <path class="st3" d="M7.9,4.6l1.6-2.8c0,0,0.5-0.8,1.6-0.8s1.7,0.8,1.7,1.7c0,1.1-1.6,1.9-1.6,1.9"/>
               </svg>
               <?php _e('Give a Gift', 'healthbeat'); ?>
            </a>
         </li>


                  <li class="menu-item">
            <a href="https://www.spectrumhealth.org/patient-care/services-and-treatment-detail-pages/mednow" target="_blank" class="btn-outline">


              <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
 viewBox="0 0 17 19" style="enable-background:new 0 0 17 19;" xml:space="preserve">
      <style type="text/css">
        .st-mednow{fill:#3264AB;}
      </style>
      <path class="st-mednow" d="M14.4,0.6H2.9C2.3,0.6,1.9,1,1.9,1.5v16c0,0.5,0.4,0.9,0.9,0.9h11.5c0.5,0,0.9-0.4,0.9-0.9v-16
        C15.3,1,14.9,0.6,14.4,0.6z M8.6,17.9c-0.5,0-0.9-0.4-0.9-0.9s0.4-0.9,0.9-0.9c0.5,0,0.9,0.4,0.9,0.9S9.1,17.9,8.6,17.9z M13.9,15.2
        H3.3V1.9h10.6V15.2z"/>
      </svg>
               <?php _e('MedNow', 'healthbeat'); ?>
            </a>
         </li>
          <li class="menu-item">
            <a href="https://myhealth.spectrumhealth.org/#/splash" target="_blank" class="btn-outline">
              <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                   viewBox="0 0 19 17" style="enable-background:new 0 0 19 17;" xml:space="preserve">
                   <style type="text/css">
                      .stheart1{fill:none;stroke:#3D78B8;stroke-width:1.9581;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;}
                      .stheart2{fill:none;stroke:#3D78B8;stroke-width:1.6267;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;}
                   </style>
                   <g>
                      <path class="stheart1" d="M9.5,15.6L2.3,8.4c-1.5-1.5-1.5-4,0-5.5l0,0c1.5-1.5,4-1.5,5.5,0l1.7,1.7l0,0L11.2,3c1.5-1.5,4-1.5,5.5,0l0,0
                         c1.5,1.5,1.5,4,0,5.5L9.5,15.6L9.5,15.6z"/>
                      <polyline class="stheart2" points="7,7.8 9.5,10.3 14.2,5.7 	"/>
                   </g>
                </svg>
               <?php _e('MyHealth', 'healthbeat'); ?>
            </a>
         </li>

      </ul>
   </div>
   <?php dynamic_sidebar('main-sidebar'); ?>
   <?php if(is_single()) : ?>
   <?php if(function_exists('wp_related_posts')) : ?>
   <!-- Related Posts -->
   <div class="widget-area">
      <h2 class="widget-title"><?php _e('Related Posts', 'healthbeat'); ?></h2>
      <?php wp_related_posts(); ?>
   </div>
   <!-- End Related Posts -->
   <?php endif; ?>
   <?php if(function_exists('wpp_get_mostpopular')) : ?>
   <!-- Popular Posts -->
   <div class="widget-area">
      <h2 class="widget-title"><?php _e('Popular Posts', 'healthbeat'); ?></h2>
      <?php
         $wpp_args = array(
         	'limit' => 10,
         	'range' => 'monthly',
         	'freshness' => 1,
         	'post_type' => array('post', 'infographics', 'patient-stories', 'videos'),
         	'wpp_start' => '<ol>',
         	'wpp_end' => '</ol>',
         	'post_html' => '<li><a href="{url}">{text_title}</a></li>'
         );
         	wpp_get_mostpopular($wpp_args);
         ?>
   </div>
   <!-- End Popular Posts -->
   <!-- Post Tags -->
   <?php
      $tag_list = get_the_tag_list('<ul><li>','</li><li>','</li></ul>');

      if($tag_list) :
      ?>
   <div class="widget-area">
      <h2 class="widget-title"><?php _e('Tags', 'healthbeat'); ?></h2>
      <div class="post-tags">
         <?php echo $tag_list; ?>
      </div>
   </div>
   <?php endif; ?>
   <!-- End Post Tags -->
   <?php endif; ?>
   <?php endif; ?>
</div>
