<?php
/**
 * Individual Brand Series Landing Page
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package healthbeat
 */

get_header();

$series_slug = get_query_var('term');

$series_data = get_term_by('slug', $series_slug, 'brand-series');
$series_id = $series_data->term_id;

$series_selector = "brand-series_" . $series_id;
$series_image = get_field('series_home_banner', $series_selector);
?>

<div id="brand-series" class="container-fluid page">
	<div class="col-sm-9 no-right-padding">

		<div class="col-md-12">
			<div class="page-header-container series-header">

				<div class="series-image" style="background: url('<?php echo $series_image; ?>') no-repeat center center; background-size: cover;">
					<div class="series-flag">
						<?php echo sprintf('A <span>%s</span> Special Series', __('HealthBeat', 'healthbeat')); ?>
					</div>

					<div class="series-content">
						<h1><?php echo $series_data->name; ?></h1>
						<p>
							<?php echo term_description($series_id, 'brand-series'); ?>
						</p>
					</div>
				</div>
			</div>
		</div>

      <div class="clear clearfix"></div>
      <?php if(have_posts()) : ?>
        <div class="posts">
          <?php while(have_posts()) : the_post(); ?>
            <?php include(locate_template(Healthbeat_Helper::$parts . 'components/post-list.php')); ?>
          <?php endwhile; ?>

          <?php healthbeat_pagination(); ?>
        </div>
      <?php endif; ?>
	</div>
	<div class="col-sm-3">
		<?php get_sidebar('main-sidebar'); ?>
	</div>
</div><!-- .container -->

<?php
get_footer();
