<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package healthbeat
 */

get_header();

$curauth = (isset($_GET['author_name'])) ? get_user_by('slug', $author_name) : get_userdata(intval($author));

$avatar = get_avatar($curauth->ID);
?>

<div id="author" class="container-fluid page">
	<div class="col-sm-9 no-right-padding">

		<div class="author-header page-header-container">
      <div class="<?php echo($avatar ? 'col-md-8' : 'col-md-12'); ?>">
        <h1><?php _e('Author:', 'healthbeat'); ?></h1>
        <h2><?php echo $curauth->display_name; ?></h2>

        <p><?php echo $curauth->description; ?></p>
      </div>

      <?php if($avatar) : ?>
        <div class="col-md-4">
          <div class="page-header-img">
            <?php echo get_avatar($curauth->ID, 320); ?>
          </div>
        </div>
      <?php endif; ?>
    </div>

    <div class="clear clearfix"></div>

    <div class="col-md-12">
      <h4><?php _e('Posts from', 'healthbeat'); ?> <?php echo $curauth->display_name; ?></h4>
    </div>

			<?php

			if(have_posts()) {
				while(have_posts()) {
					the_post();
					include(locate_template(Healthbeat_Helper::$parts . 'components/post-list.php'));
				}

				healthbeat_pagination();
			}

			?>

	</div>
	<div class="col-sm-3">
		<?php get_sidebar('main-sidebar'); ?>
	</div>
</div><!-- .container -->

<?php
get_footer();
