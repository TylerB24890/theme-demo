<?php

/**
 * @link              http://elexicon.com
 * @since             0.1
 * @package           Healthbeat
 *
 * This script is normally setup as a plugin for this theme, however for demo purposes I have included it within the theme itself.
 */


define('HB_DIR', get_template_directory() . '/inc/healthbeat/');
define('HB_PLUGIN_URL', get_template_directory_uri() . '/inc/healthbeat/');


/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require HB_DIR . 'inc/class-healthbeat.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    0.1.0
 */
if(!function_exists('run_healthbeat')) {
	function run_healthbeat() {
		$plugin = new Healthbeat();
	}
	run_healthbeat();
}
