<?php
/**
 * Creates custom Health Beat RSS Feeds
 *
 * @author Elexicon, Inc.
 * @version 1.0.0
 */

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

if(!class_exists('Healthbeat_RSS')) :

	class Healthbeat_RSS {
		public function __construct() {
			$this->register_hb_feeds();
		}

		/**
		 * Register all custom RSS feeds
		 */
		private function register_hb_feeds() {
			add_action( 'after_setup_theme', 'Healthbeat_RSS::email_rss_template' );
			add_action( 'after_setup_theme', 'Healthbeat_RSS::industry_weapon_rss_template' );
			add_filter("the_author",'Healthbeat_RSS::hb_author_rss');
		}

		/**
		 * Register Email Feed for Health Beat digest
		 */
		public static function email_rss_template() {
			add_feed( 'email', 'Healthbeat_RSS::email_rss_render' );
		}
		public static function email_rss_render() {
			include_once(HB_DIR . 'inc/rss/feed-email.php');
		}

		/**
		 * Register Industry Weapon RSS Feed
		 */
		public static function industry_weapon_rss_template() {
 			add_feed( 'industry-weapon', 'Healthbeat_RSS::industry_weapon_rss_render' );
 		}
 		public static function industry_weapon_rss_render() {
 			include_once(HB_DIR . 'inc/rss/feed-industry-weapon.php');
 		}

		/**
		 * Create author RSS Feed
		 * @param  int $author Post Author ID
		 * @return  string         Replaced author with post excerpt
		 */
		public static function hb_author_rss($author) {
		    global $post;

		    if(is_feed()) {
		        $author = the_excerpt();
		    }
		    return $author;
		}
	}

	new Healthbeat_RSS();

endif;
