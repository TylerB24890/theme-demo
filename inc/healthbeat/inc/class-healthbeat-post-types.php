<?php

/**
 * Custom post types for the healthbeat theme
 *
 * @author Elexicon, Inc.
 * @version 1.0.0
 */

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

if( !class_exists('Healthbeat_Post_Types') ) :

	class Healthbeat_Post_Types {

		public function __construct() {
			add_action( 'init', 'Healthbeat_Post_Types::post_type_infographics' );
			add_action( 'init', 'Healthbeat_Post_Types::post_type_patient_stories' );
			add_action( 'init', 'Healthbeat_Post_Types::post_type_videos' );
			add_action( 'init', 'Healthbeat_Post_Types::post_type_events' );
			add_action( 'init', 'Healthbeat_Post_Types::post_type_promos' );
			add_action( 'init', 'Healthbeat_Post_Types::post_type_polls' );
		}

		public static function post_type_infographics() {
			$labels = array(
			    'name'               => _x( 'Infographics', 'post type general name' ),
			    'singular_name'      => _x( 'Infographic', 'post type singular name' ),
			    'add_new'            => _x( 'Add New', 'infographic' ),
			    'add_new_item'       => __( 'Add New Infographic' ),
			    'edit_item'          => __( 'Edit Infographic' ),
			    'new_item'           => __( 'New Infographic' ),
			    'all_items'          => __( 'All Infographics' ),
			    'view_item'          => __( 'View Infographic' ),
			    'search_items'       => __( 'Search Infographics' ),
			    'not_found'          => __( 'No Infographics found' ),
			    'not_found_in_trash' => __( 'No Infographics found in the Trash' ),
			    'parent_item_colon'  => '',
			    'menu_name'          => 'Infographics'
			);
			$args = array(
			    'labels'        => $labels,
			    'description'   => 'Holds our Infographics and Infographic specific data',
			    'public'        => true,
			    'menu_position' => 5,
			    'taxonomies' 	=> array('category', 'hospitals', 'post_tag', 'secondary_tags', 'series'),
			    'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments', 'author'),
			    'has_archive'   => true,
			);
			register_post_type( 'infographics', $args );
			flush_rewrite_rules();
		}

		public static function post_type_patient_stories() {
			$labels = array(
			    'name'               => _x( 'Patient Stories', 'post type general name' ),
			    'singular_name'      => _x( 'Patient Story', 'post type singular name' ),
			    'add_new'            => _x( 'Add New', 'patient story' ),
			    'add_new_item'       => __( 'Add New Patient Story' ),
			    'edit_item'          => __( 'Edit Patient Story' ),
			    'new_item'           => __( 'New Patient Story' ),
			    'all_items'          => __( 'All Patient Stories' ),
			    'view_item'          => __( 'View Patient Story' ),
			    'search_items'       => __( 'Search Patient Stories' ),
			    'not_found'          => __( 'No Patient Stories found' ),
			    'not_found_in_trash' => __( 'No Patient Stories found in the Trash' ),
			    'parent_item_colon'  => '',
			    'menu_name'          => 'Patient Stories'
			);
			$args = array(
			    'labels'        => $labels,
			    'description'   => 'Holds our Patient Story specific data',
			    'public'        => true,
			    'menu_position' => 6,
			    'taxonomies' 	=> array('category', 'hospitals', 'post_tag', 'secondary_tags', 'series'),
			    'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments', 'author' ),
			    'has_archive'   => true,
			);
			register_post_type( 'patient-stories', $args );
			flush_rewrite_rules();
		}

		public static function post_type_videos() {
			$labels = array(
			    'name'               => _x( 'Videos', 'post type general name' ),
			    'singular_name'      => _x( 'Video', 'post type singular name' ),
			    'add_new'            => _x( 'Add New', 'video' ),
			    'add_new_item'       => __( 'Add New Video' ),
			    'edit_item'          => __( 'Edit Video' ),
			    'new_item'           => __( 'New Video' ),
			    'all_items'          => __( 'All Videos' ),
			    'view_item'          => __( 'View Video' ),
			    'search_items'       => __( 'Search Videos' ),
			    'not_found'          => __( 'No Videos found' ),
			    'not_found_in_trash' => __( 'No Videos found in the Trash' ),
			    'parent_item_colon'  => '',
			    'menu_name'          => 'Videos'
			);
			$args = array(
			    'labels'        => $labels,
			    'description'   => 'Holds our Video specific data',
			    'public'        => true,
			    'menu_position' => 7,
			    'taxonomies' 	=> array('category', 'hospitals', 'post_tag', 'secondary_tags', 'series'),
			    'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments', 'author' ),
			    'has_archive'   => true,
			);
			register_post_type( 'videos', $args );
			flush_rewrite_rules();
		}

		public static function post_type_events() {
			$labels = array(
			    'name'               => _x( 'Events', 'post type general name' ),
			    'singular_name'      => _x( 'Event', 'post type singular name' ),
			    'add_new'            => _x( 'Add New', 'Event' ),
			    'add_new_item'       => __( 'Add New Event' ),
			    'edit_item'          => __( 'Edit Event' ),
			    'new_item'           => __( 'New Event' ),
			    'all_items'          => __( 'All Events' ),
			    'view_item'          => __( 'View Event' ),
			    'search_items'       => __( 'Search Events' ),
			    'not_found'          => __( 'No Events found' ),
			    'not_found_in_trash' => __( 'No Events found in the Trash' ),
			    'parent_item_colon'  => '',
			    'menu_name'          => 'Events'
			);
			$args = array(
			    'labels'        => $labels,
			    'description'   => 'Holds our Events and Event specific data',
			    'public'        => true,
			    'menu_position' => 5,
			    'taxonomies' 	=> array('category', 'hospitals', 'post_tag', 'secondary_tags', 'series'),
			    'supports'      => array( 'title', 'thumbnail'),
			    'has_archive'   => false,
			);
			register_post_type( 'events', $args );
			flush_rewrite_rules();
		}

		public static function post_type_promos() {
			$labels = array(
			    'name'               => _x( 'Promos', 'post type general name' ),
			    'singular_name'      => _x( 'Promo', 'post type singular name' ),
			    'add_new'            => _x( 'Add New', 'Promo' ),
			    'add_new_item'       => __( 'Add New Promo' ),
			    'edit_item'          => __( 'Edit Promo' ),
			    'new_item'           => __( 'New Promo' ),
			    'all_items'          => __( 'All Promos' ),
			    'view_item'          => __( 'View Promo' ),
			    'search_items'       => __( 'Search Promos' ),
			    'not_found'          => __( 'No Promos found' ),
			    'not_found_in_trash' => __( 'No Promos found in the Trash' ),
			    'parent_item_colon'  => '',
			    'menu_name'          => 'Promos'
			);
			$args = array(
			    'labels'        => $labels,
			    'description'   => 'Holds our Promos and Promo specific data',
			    'public'        => true,
			    'menu_position' => 4,
			    'taxonomies' 	=> array('category', 'hospitals', 'post_tag', 'secondary_tags', 'expert_blog'),
			    'supports'      => array( 'title', 'thumbnail'),
			    'has_archive'   => false,
			);
			register_post_type( 'promos', $args );
			flush_rewrite_rules();
		}

		public static function post_type_polls() {
			$labels = array(
			    'name'               => _x( 'Polls', 'post type general name' ),
			    'singular_name'      => _x( 'Poll', 'post type singular name' ),
			    'add_new'            => _x( 'Add New', 'Poll' ),
			    'add_new_item'       => __( 'Add New Poll' ),
			    'edit_item'          => __( 'Edit Poll' ),
			    'new_item'           => __( 'New Poll' ),
			    'all_items'          => __( 'All Polls' ),
			    'view_item'          => __( 'View Poll' ),
			    'search_items'       => __( 'Search Polls' ),
			    'not_found'          => __( 'No Polls found' ),
			    'not_found_in_trash' => __( 'No Polls found in the Trash' ),
			    'parent_item_colon'  => '',
			    'menu_name'          => 'Polls'
			);
			$args = array(
			    'labels'        => $labels,
			    'description'   => 'Holds our Polls and Poll specific data',
			    'public'        => true,
			    'menu_position' => 4,
			    'taxonomies' 	=> array('category', 'hospitals', 'post_tag', 'secondary_tags', 'expert_blog'),
			    'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
			    'has_archive'   => false,
			);
			register_post_type( 'polls', $args );
			flush_rewrite_rules();
		}
	}

	new Healthbeat_Post_Types();
endif;
