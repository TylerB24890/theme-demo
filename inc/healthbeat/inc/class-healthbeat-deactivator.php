<?php
/**
 * Fired during plugin deactivation
 *
 * @author Elexicon, Inc.
 * @version 1.0.0
 */

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

if(!class_exists('Healthbeat_Deactivator')) :

	class Healthbeat_Deactivator {
		public static function deactivate() {
			return;
		}
	}

endif;
