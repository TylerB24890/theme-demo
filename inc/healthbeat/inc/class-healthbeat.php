<?php

/**
 * healthbeat Theme initialization class
 *
 * Initial theme setup and functions
 *
 * @author Elexicon, Inc.
 * @version 1.0.0
 */

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

if( !class_exists('Healthbeat') ) :

	class Healthbeat {

		/**
		 * Global theme slug
		 */
		public static $theme_slug;

		/**
		 * Global theme prefix
		 */
		public static $theme_prefix;

		/**
		 * Global theme version
		 */
		public static $theme_version;

		/**
		 * Constructs the class variables
		 */
		public function __construct() {
			self::$theme_slug = 'healthbeat';
			self::$theme_prefix = 'healthbeat';
			self::$theme_version = '1.1.0';

			$this->load_dependencies();
		}

		/**
		 * Loads the required files for the healthbeat theme to function
		 *
		 * @return null
		 */
		public function load_dependencies() {
			foreach(glob(HB_DIR . 'inc/*.php') as $file) {
				if($file !== 'class-healthbeat-activator.php' && $file !== 'class-healthbeat-deactivator.php') {
					include_once($file);
				}
			}

			if(is_admin())
				include_once(HB_DIR . 'inc/admin/class-healthbeat-admin.php');
		}
	}

endif;
