<?php
/**
 * Health Beat Widget Areas
 *
 * @package healthbeat
 * @subpackage	healthbeat/inc
 */

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

if(!class_exists('Healthbeat_Widgets')) :

	class Healthbeat_Widgets {
		public function __construct() {
			add_action( 'widgets_init', 'Healthbeat_Widgets::register_hb_widgets' );
		}

		public static function register_hb_widgets() {
			register_sidebar( array(
				'name' => 'Offcanvas',
				'id' => 'offcanvas',
				'before_widget' => '<div class="nav-section">',
				'after_widget' => '</div>',
				'before_title' => '<h2 class="nav-title">',
				'after_title' => '</h2>'
			) );

			register_sidebar( array(
				'name' => 'Main Sidebar',
				'id' => 'main-sidebar',
				'before_widget' => '<div class="widget-area">',
				'after_widget' => '</div>',
				'before_title' => '<h2 class="widget-title">',
				'after_title' => '</h2>'
			) );

			register_sidebar( array(
				'name' => 'Footer Column 1',
				'id' => 'footer-column-one',
				'before_widget' => '<div class="footer-column">',
				'after_widget' => '</div>',
				'before_title' => '<h2 class="column-title">',
				'after_title' => '</h2><hr />'
			) );

			register_sidebar( array(
				'name' => 'Footer Column 2',
				'id' => 'footer-column-two',
				'before_widget' => '<div class="footer-column">',
				'after_widget' => '</div>',
				'before_title' => '<h2 class="column-title">',
				'after_title' => '</h2><hr />'
			) );

			register_sidebar( array(
				'name' => 'Footer Column 3',
				'id' => 'footer-column-three',
				'before_widget' => '<div class="footer-column">',
				'after_widget' => '</div>',
				'before_title' => '<h2 class="column-title">',
				'after_title' => '</h2><hr />'
			) );

			register_sidebar( array(
				'name' => 'Footer Column 4',
				'id' => 'footer-column-four',
				'before_widget' => '<div class="footer-column">',
				'after_widget' => '</div>',
				'before_title' => '<h2 class="column-title">',
				'after_title' => '</h2><hr />'
			) );

			register_sidebar( array(
				'name' => 'Footer Column 5',
				'id' => 'footer-column-five',
				'before_widget' => '<div class="footer-column">',
				'after_widget' => '</div>',
				'before_title' => '<h2 class="column-title">',
				'after_title' => '</h2><hr />'
			) );
		}

		/**
		 * Compare the selected promo categories with the post categories
		 * @param  array $cats Array of selected promo categories
		 * @param  int $post_cat ID of category of post
		 * @return bool True or false if the promo is in the correct category
		 */
		public static function check_promo_category($cats, $post_cat) {
			$display = false;

			if($cats && !empty($cats)) {
				if(in_array($post_cat, $cats)) {
					$display = true;
				}
			} else {
				$display = true;
			}

			return $display;
		}
	}

	new Healthbeat_Widgets();

endif;
