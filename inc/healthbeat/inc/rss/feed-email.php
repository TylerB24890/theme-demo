<?php
/**
 * Health Beat Email RSS template with related posts.
 *
 * @package healthbeat
 * @author	Tyler Bailey
 *
 */

wp_reset_query();

/**
 * Feed defaults.
 */
header( 'Content-Type: ' . feed_content_type( 'rss-http' ) . '; charset=' . get_option( 'blog_charset' ), true );
$frequency  = 1;
$duration   = 'hourly';

/**
 * Start RSS feed.
 */
echo '<?xml version="1.0" encoding="' . get_option( 'blog_charset' ) . '"?' . '>'; ?>
<!DOCTYPE text [ <!ENTITY hellip "&#x2026;"> ]>
<rss version="2.0"
	xmlns:content="http://purl.org/rss/1.0/modules/content/"
	xmlns:wfw="http://wellformedweb.org/CommentAPI/"
	xmlns:dc="http://purl.org/dc/elements/1.1/"
	xmlns:atom="http://www.w3.org/2005/Atom"
	xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
	xmlns:slash="http://purl.org/rss/1.0/modules/slash/"
	<?php do_action( 'rss2_ns' ); ?>
>

  <!-- RSS feed defaults -->
	<channel>
		<title><?php bloginfo_rss( 'name' ); wp_title_rss(); ?></title>
		<link><?php bloginfo_rss( 'url' ) ?></link>
		<description><?php bloginfo_rss( 'description' ) ?></description>
		<lastBuildDate><?php echo mysql2date( 'D, d M Y H:i:s +0000', get_lastpostmodified( 'GMT' ), false ); ?></lastBuildDate>
		<language><?php bloginfo_rss( 'language' ); ?></language>
		<sy:updatePeriod><?php echo apply_filters( 'rss_update_period', $duration ); ?></sy:updatePeriod>
		<sy:updateFrequency><?php echo apply_filters( 'rss_update_frequency', $frequency ); ?></sy:updateFrequency>
		<atom:link href="<?php self_link(); ?>" rel="self" type="application/rss+xml" />

		<!-- Feed Logo (optional) -->
		<image>
			<url><?php echo get_template_directory_uri() . '/images/placeholder_200x200.jpg'; ?></url>
			<title>
				<?php bloginfo_rss( 'title' ) ?>
			</title>
			<link><?php bloginfo_rss( 'url' ) ?></link>
		</image>

		<?php do_action( 'rss2_head' ); ?>


		<?php while( have_posts()) : the_post(); ?>

			<?php

				// Get Guest Author
				$guestAuthor = get_field('guest_author_name');

				// If there is a guest author, use that field
				if(strlen($guestAuthor) > 1) {
					$authorName = $guestAuthor;
				} else {
					// no guest author, get actual author
					$authorID = $post->post_author;
					$authorName = get_userdata($authorID)->display_name;
				}

				$postImage = get_field('social_image', $post->ID);

				if(!$postImage || strlen($postImage) < 5) {
					// Get post featured image URL
					$postImages = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' );

					// Featured Image or Fallback
					if ( $postImages ) {
						$postImage = $postImages[0];
					} else {
						$postImage = get_template_directory_uri() . '/images/placeholder_200x200.jpg';
					}
				}

				$postOutput = preg_replace('/<img[^>]+./','', get_the_content($post->ID));
				$postOutput = strip_shortcodes($postOutput);



			?>

			<item>
				<title><?php the_title_rss(); ?></title>
				<link><?php the_permalink_rss(); ?></link>
				<guid isPermaLink="false"><?php the_guid(); ?></guid>
				<copyright><?php echo $authorName; ?></copyright>
				<author><?php echo the_excerpt_rss(); ?></author>
				<?php the_category_rss('rss2') ?>
				<image>
					<url><?php echo esc_url( $postImage ); ?></url>
				</image>
				<pubDate><?php echo mysql2date( 'D, d M Y H:i:s +0000', get_post_time( 'Y-m-d H:i:s', true ), false ); ?></pubDate>
				<content:encoded>
					<![CDATA[<?php echo strip_tags($postOutput); ?>]]>
				</content:encoded>
			</item>

		<?php endwhile; ?>
	</channel>
</rss>
