<?php

/**
 * Functions to assist with Health Beat author data
 *
 * @author Elexicon, Inc.
 * @version 1.0.0
 */

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

if( !class_exists('Healthbeat_Authors') ) :

	class Healthbeat_Authors {

		/**
		 * Get post author data for displaying in post blocks
		 * @param  int $pid Post ID
		 * @return array Array of author data (name, ID, image)
		 */
		public static function get_post_author_data($pid = 0) {
			if($pid === 0) {
				global $post;
				$pid = $post->ID;
			}

			$author = array();
			$guest = get_field('guest_author_name', $pid);

			if(!isset($guest) || empty($guest) || strlen($guest) < 2) {
				$author['id'] = get_post_field('post_author', $pid);
				$author['name'] = get_the_author_meta('display_name', $author['id']);
				$author['url'] = get_author_posts_url($author['id']);
			} else {
				$author['id'] = 0;
				$author['name'] = get_field('guest_author_name');
			}

			$author['image'] = get_avatar($author['id']);

			return $author;
		}

    /**
     * Return Health Beat authors
     * @param  [type] $args [description]
     * @return [type]       [description]
     */
    public static function author_loop($args) {

      $users = get_users($args);

      if($args['orderby'] !== 'ID') {
        // Sort Authors by Last Name
        usort($users, create_function('$a, $b', 'return strnatcasecmp($a->last_name, $b->last_name);'));
      }

      // Loop through the first author query
      foreach($users as $author) {
        if(count_user_posts($author->ID) > 0) {
          echo '<div class="col-md-4">';
            include(locate_template(Healthbeat_Helper::$parts . 'components/author-block.php'));
          echo '</div>';
        }
      }
    }
	}

	new Healthbeat_Authors();
endif;
