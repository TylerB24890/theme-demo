<?php

/**
 * Functions to assist with the WP Loop/Post grabbing
 *
 * @author Elexicon, Inc.
 * @version 1.0.0
 */

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

if( !class_exists('Healthbeat_Posts') ) :

	class Healthbeat_Posts {

		/**
		 * Get all post data for a given ID
		 * @param  int $post_id Post ID
		 * @return array Array of Post Data
		 */
		public static function get_post_data($post_id) {
			$post_data = array();

			$current_category = false;
			$photo_gallery = false;
			$special_series = false;
			$brand_series = false;
			$patient_story = false;

			$post = get_post($post_id);

			if(is_category()) {
			  $current_category = get_queried_object_id();
			} else {
				$current_category = self::get_post_category_flag($post_id);
			}

			if(is_single()) {
				$photo_gallery = get_field('photo_gallery', $post_id);
				$special_series = get_the_terms($post_id, 'special-series');
				$brand_series = get_the_terms($post_id, 'brand-series');

				if($post->post_type == 'patient-stories' || get_field('patient_story', $post_id)) {
					$patient_story = true;
				}
			}

			$post_data['post_type'] = $post->post_type;
			$post_data['id'] = $post_id;
			$post_data['title'] = $post->post_title;
			$post_data['slug'] = $post->post_name;
			$post_data['excerpt'] = $post->post_excerpt;
			$post_data['category'] = $current_category;
			$post_data['photo_gallery'] = $photo_gallery;
			$post_data['special_series'] = $special_series;
			$post_data['patient_story'] = $patient_story;
			$post_data['brand_series'] = $brand_series;
			$post_data['author'] = Healthbeat_Authors::get_post_author_data($post_id);
			$post_data['block_type'] = self::get_post_block_type($post_id);
			$post_data['url'] = get_the_permalink($post_id);
			$post_data['date'] = get_the_date('F j, Y', $post_id);
			$post_data['content'] = apply_filters('the_content', $post->post_content);

			return $post_data;
		}

		/**
		 * Return the post category data for flag creation
		 * @param  int $pid Post ID
		 * @return array array of category data
		 */
		private static function get_post_category_flag($pid = false) {
			if(!$pid)
				return;

			$return_arr = array();
			$categories = get_the_category($pid);
			$cat_id = 0;
			$cat_name = '';
			$cat_url = '';
			$cat_slug = 'hide';
			$parent_cat = 0;
			$child_slug = '';

			if(!empty($categories)) {
				$cat_id = $categories[0]->term_id;
				$cat_name = esc_html($categories[0]->name);
				$cat_url = esc_url(get_category_link($cat_id));
				$cat_slug = $categories[0]->slug;

				$parent_cat = $categories[0]->category_parent;

				if($parent_cat != 0) {
					$cat_id = $parent_cat;
					$parent_cat_data = get_category($parent_cat);
					$cat_slug = $parent_cat_data->slug;
					$child_slug = $categories[0]->slug;
				}

				$return_arr['id'] = $cat_id;
				$return_arr['name'] = $cat_name;
				$return_arr['slug'] = $cat_slug;
				$return_arr['child'] = $child_slug;
				$return_arr['url'] = $cat_url;
			}

			return $return_arr;
		}

		/**
		 * Return post block type
		 * @param  int $pid Post ID
		 * @return string Name of the post type for displaying in post list
		 */
		private static function get_post_block_type($pid = false) {
			if(!$pid)
				return;

			$post_type = get_post_type($pid);
			$block_type = '';

			// Display Post Type
			switch($post_type) {
				case 'infographics' :
					$block_type = 'Infographic';
				break;
				case 'polls' :
					$block_type = 'Poll';
				break;
				case 'patient-stories' :
					$block_type = 'Patient Story';
				break;
				case 'videos' :
					$block_type = 'Video';
				break;
				case 'post' :
					$is_patient_story = get_field('patient_story', $pid);

					if($is_patient_story == '1' && get_the_ID() != "12395") {
						$block_type = 'Patient Story';
					}
				break;
			}
			return $block_type;
		}

		/**
		 * Use 'get_the_content()' and apply formatting
		 */
		public static function get_the_content_with_formatting ($more_link_text = '(more...)', $stripteaser = 0, $more_file = '') {
			$content = get_the_content($more_link_text, $stripteaser, $more_file);
			$content = apply_filters('the_content', $content);
			$content = str_replace(']]>', ']]&gt;', $content);
			return $content;
		}

		/**
		 * Render the Healthbeat Single Post Content
		 * @param  array $post_data 	Array of post data
		 * @return string							HTML markup of post content
		 */
		public static function render_post($post_data) {
			if(!is_array($post_data))
				return;

			include(locate_template(Healthbeat_Helper::$parts . 'single/single-header.php'));

			if(get_field('photo_gallery')) {
				include(locate_template(Healthbeat_Helper::$parts . 'single/single-photo-gallery.php'));
			}

			include(locate_template(Healthbeat_Helper::$parts . 'single/single-byline.php'));

			include(locate_template(Healthbeat_Helper::$parts . 'single/single-content.php'));
		}

		/**
		 * Render the CTAs at the bottom of single posts (if necessary)
		 * @param  object $post global WP Post Object
		 * @return string       HTML markup for post CTAs
		 */
		public static function render_post_ctas($post) {

			$cta = get_field('call_to_action', $post->ID);

			if($cta && strlen($cta) > 0) {
				echo '<div class="cta single-cta">' . $cta . '</div>';
			}

			$series_term = get_the_terms($post->ID, 'special-series');
			if(is_array($series_term) && !empty($series_term)) {
				$series_selector = $series_term[0]->taxonomy . "_" . $series_term[0]->term_id;
				$series_banner = get_field('series_banner', $series_selector);
			}

			if($series_banner && strlen($series_banner) > 1) : ?>
				<a href="/series/<?php echo $series_term[0]->slug; ?>" class="series-cta">
					<img class="aligncenter size-full" src="<?php echo $series_banner; ?>">
				</a>
			<?php
			endif;

			$brand_term = get_the_terms($post->ID, 'brand-series');
			if(is_array($brand_term) && !empty($brand_term)) {
				$brand_selector = $brand_term[0]->taxonomy . "_" . $brand_term[0]->term_id;
				$brand_banner = get_field('series_post_banner', $brand_selector);
			}
			if($brand_banner && strlen($brand_banner) > 1) : ?>
				<a href="/inspire-innovate-impact/<?php echo $brand_term[0]->slug; ?>/" class="series-cta">
					<img class="aligncenter size-full" src="<?php echo $brand_banner; ?>">
				</a>
			<?php
			endif;
		}
	}

	new Healthbeat_Posts();
endif;
