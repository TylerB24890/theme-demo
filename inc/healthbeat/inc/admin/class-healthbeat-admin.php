<?php
/**
 * Customizes the Health Beat wp-admin dashboard
 *
 * @author Elexicon, Inc.
 * @version 1.0.0
 */

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

if(!class_exists('Healthbeat_Admin')) :

	class Healthbeat_Admin {

		public function __construct() {
			add_action( 'login_enqueue_scripts', 'Healthbeat_Admin::login_logo' );

			if(is_admin()) {
				add_action( 'admin_menu', 'Healthbeat_Admin::add_story_budget_link' );
				add_action( 'admin_init', 'Healthbeat_Admin::limit_content_strategist' );
				add_action( 'admin_init', 'Healthbeat_Admin::story_budget_link_redirect', 1 );
				add_action( 'admin_head', 'Healthbeat_Admin::rename_infographic_featured' );
				add_action( 'admin_head',  'Healthbeat_Admin::change_excerpt_box_title' );
				add_action( 'admin_head',  'Healthbeat_Admin::change_category_box_title' );
				add_action( 'admin_head',  'Healthbeat_Admin::change_tag_box_title' );
				add_action( 'admin_head', 'Healthbeat_Admin::custom_excerpt_count');
				add_action( 'admin_footer', 'Healthbeat_Admin::catlist2radio' );
				add_action( 'admin_footer', 'Healthbeat_Admin::active_featured_article' );
			}

			include_once(HB_DIR . 'inc/admin/class-healthbeat-admin-categories.php');
		}

		/**
		 *	Change wp-login logo
		 */
		public static function login_logo() {
		?>
		    <style type="text/css">
		        body.login div#login h1 a {
		            background-image: url(<?php echo HB_PLUGIN_URL; ?>inc/admin/assets/img/sh-logo.png);
		            background-size: 100%;
		            width: 230px;
		            height: 51px;
		        }
		    </style>
		<?php
		}

		/**
		 *	Rename 'Featured Image' on Infographics
		 */
		public static function rename_infographic_featured() {
		    remove_meta_box( 'postimagediv', 'infographics', 'side' );
		    add_meta_box('postimagediv', __('Full Infographic'), 'post_thumbnail_meta_box', 'infographics', 'side', 'high');
		}

		/**
		 *	Rename 'Excerpt' to 'Teaser'
		 */
		public static function change_excerpt_box_title() {
			/* Post */
			remove_meta_box( 'postexcerpt', 'post', 'core' );
			add_meta_box('postexcerpt', __('Teaser'), 'post_excerpt_meta_box', 'post', 'normal', 'high');
			/* Infographics */
			remove_meta_box( 'postexcerpt', 'infographics', 'core' );
			add_meta_box('postexcerpt', __('Teaser'), 'post_excerpt_meta_box', 'infographics', 'normal', 'high');
			/* Patient Stories */
			remove_meta_box( 'postexcerpt', 'patient-stories', 'core' );
			add_meta_box('postexcerpt', __('Teaser'), 'post_excerpt_meta_box', 'patient-stories', 'normal', 'high');
			/* Videos */
			remove_meta_box( 'postexcerpt', 'videos', 'core' );
			add_meta_box('postexcerpt', __('Teaser'), 'post_excerpt_meta_box', 'videos', 'normal', 'high');
		}

		/**
		 *	Rename 'Categories' to 'Category'
		 */
		public static function change_category_box_title() {
			/* Post */
			remove_meta_box( 'categorydiv', 'post', 'side' );
			add_meta_box('categorydiv', __('Category'), 'post_categories_meta_box', 'post', 'normal', 'high');
			/* Infographics */
			remove_meta_box( 'categorydiv', 'infographics', 'side' );
			add_meta_box('categorydiv', __('Category'), 'post_categories_meta_box', 'infographics', 'normal', 'high');
			/* Patient Stories */
			remove_meta_box( 'categorydiv', 'patient-stories', 'side' );
			add_meta_box('categorydiv', __('Category'), 'post_categories_meta_box', 'patient-stories', 'normal', 'high');
			/* Videos */
			remove_meta_box( 'categorydiv', 'videos', 'side' );
			add_meta_box('categorydiv', __('Category'), 'post_categories_meta_box', 'videos', 'normal', 'high');
		}

		/**
		 *	Rename 'Tags' to 'Keywords'
		 */
		public static function change_tag_box_title() {
			/* Post */
			remove_meta_box( 'tagsdiv-post_tag', 'post', 'side' );
			add_meta_box('tagsdiv-post_tag', __('Keywords'), 'post_tags_meta_box', 'post', 'normal', 'high');
			/* Infographics */
			remove_meta_box( 'tagsdiv-post_tag', 'infographics', 'side' );
			add_meta_box('tagsdiv-post_tag', __('Keywords'), 'post_tags_meta_box', 'infographics', 'normal', 'high');
			/* Patient Stories */
			remove_meta_box( 'tagsdiv-post_tag', 'patient-stories', 'side' );
			add_meta_box('tagsdiv-post_tag', __('Keywords'), 'post_tags_meta_box', 'patient-stories', 'normal', 'high');
			/* Videos */
			remove_meta_box( 'tagsdiv-post_tag', 'videos', 'side' );
			add_meta_box('tagsdiv-post_tag', __('Keywords'), 'post_tags_meta_box', 'videos', 'normal', 'high');
		}

		/**
		 * Add Custom Admin Menu Link to Story Budget Page
		 */
		public static function add_story_budget_link(){
		    add_menu_page( 'Story Budget', 'Story Budget', 'edit_others_posts', 'story-budget', 'function', 'dashicons-format-aside', 4 );
		}

		/**
		 * Redirect Story Budget Slug to Appropriate Story Budget Post
		 */
		public static function story_budget_link_redirect() {

		    $menu_redirect = isset($_GET['page']) ? $_GET['page'] : false;

		    if($menu_redirect == 'story-budget' ) {
		        wp_redirect( admin_url('post.php?post=14017&action=edit') );
		        //wp_redirect( 'http://healthbeat.spectrumhealth.org/wp-admin/post.php?post=14017&action=edit' );
		        exit();
		    }
		}

		/***
		 * Remove all menu items EXCEPT Story Budget for the 'Content Strategist' user role
		 */
		public static function limit_content_strategist() {
			global $user_ID;

			if(current_user_can('content-strategist')) {
				remove_menu_page('edit.php'); // Posts
				remove_menu_page('upload.php'); // Media
				remove_menu_page('link-manager.php'); // Links
				remove_menu_page('edit-comments.php'); // Comments
				remove_menu_page('edit.php?post_type=page'); // Pages
				remove_menu_page('plugins.php'); // Plugins
				remove_menu_page('themes.php'); // Appearance
				remove_menu_page('users.php'); // Users
				remove_menu_page('tools.php'); // Tools
				remove_menu_page('options-general.php'); // Settings

				remove_menu_page('edit.php?post_type=infographics'); // Infographics
				remove_menu_page('edit.php?post_type=patient-stories'); // Patient Stories
				remove_menu_page('edit.php?post_type=videos'); // Videos
				remove_menu_page('edit.php?post_type=events'); // Events
				remove_menu_page('edit.php?post_type=promos'); // Promos
				remove_menu_page('edit.php?post_type=polls'); // Polls
				remove_menu_page('admin.php?page=wpcf7'); // ContactForm 7

				remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
				remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );
				remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );
				remove_meta_box( 'dashboard_secondary', 'dashboard', 'normal' );
				remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
				remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'side' );
				remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );
				remove_meta_box( 'dashboard_right_now', 'dashboard', 'normal' );
				remove_meta_box( 'dashboard_activity', 'dashboard', 'normal');//since 3.8

				add_action('admin_head', 'Healthbeat_Admin::hide_add_new_btn');
			}
		}

		/**
		 * Hide 'Add New' button for Content Strategists
		 */
		public static function hide_add_new_btn() {
			echo '<style type="text/css">a.page-title-action { display: none !important; }</style>';
		}

		/**
		 * Include the custom excerpt character count script
		 */
		public static function custom_excerpt_count() {
			if ('page' != get_post_type()) {
				include_once(HB_DIR . 'inc/admin/assets/scripts/excerpt-count.js.php');
			}
		}

		/**
		 * Convert category selection checkboxes to Radio buttons so only one can be selected
		 */
		public static function catlist2radio(){
			if ('page' != get_post_type()) {
				include_once(HB_DIR . 'inc/admin/assets/scripts/category-radios.js.php');
			}
		}

		/**
		 * Make sure only ONE 'Feautred Article' checkbox can be checked
		 */
		public static function active_featured_article() {
			if ('page' == get_post_type()) {
				include_once(HB_DIR . 'inc/admin/assets/scripts/active-featured-article.js.php');
			}
		}
	}

	new Healthbeat_Admin();

endif;
