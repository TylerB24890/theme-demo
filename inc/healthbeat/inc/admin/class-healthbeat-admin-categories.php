<?php
/**
 * Customizes the Health Beat wp-admin categories
 *
 * @author Elexicon, Inc.
 * @version 1.0.0
 */

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

if(!class_exists('Healthbeat_Admin_Categories')) :

	class Healthbeat_Admin_Categories {

		public function __construct() {
      add_action("hospitals_edit_form_fields", array($this, 'add_hospital_wysiwyg'), 10, 2);
		}

    /**
     * Convert Hospitals Description area to WYSIWYG
     * @param object $term     Hospital object
     * @param string $taxonomy Taxonomy the term belongs to
     */
    public function add_hospital_wysiwyg($term, $taxonomy){
    ?>
      <tr valign="top">
          <th scope="row">Description</th>
          <td>
              <?php wp_editor(html_entity_decode($term->description), 'description', array('media_buttons' => false)); ?>
              <script>
                  jQuery(window).ready(function(){
                      jQuery('label[for=description]').parent().parent().remove();
                  });
              </script>
          </td>
      </tr>
    <?php
    }
	}

	new Healthbeat_Admin_Categories();

endif;
