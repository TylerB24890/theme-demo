<script>

jQuery('#postexcerpt .handlediv').after('<div id="excerpt-counter" style="position:absolute;top:12px;right:34px;color:#666;"><small style="font-weight:bold; padding-right:7px;">Teaser length: </small><span id="excerpt_counter"></span><span> / 140 </span><small><span style="font-weight:bold;">character(s).</span></small></div>');

if(jQuery('textarea#excerpt').length > 0) {
	jQuery("span#excerpt_counter").text(jQuery("textarea#excerpt").val().length);
	jQuery("textarea#excerpt").keyup( function() {
		if(jQuery(this).val().length > 140){
			// Limit excerpt length to 140 characters
			//$(this).val(jQuery(this).val().substr(0, 140));
			jQuery('div#excerpt-counter').css('color', 'red');
		}
		jQuery("span#excerpt_counter").text(jQuery("textarea#excerpt").val().length);
	});
}

</script>
