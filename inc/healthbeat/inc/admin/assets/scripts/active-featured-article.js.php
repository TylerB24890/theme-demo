<script>
  (function($) {
    $('.acf-true-false input[type="checkbox"]').on('click', function() {
      $('.acf-true-false input[type="checkbox"]').prop('checked', false);
      if(!$(this).is(':checked')) {
        $(this).prop('checked', true);
      } else {
        $(this).prop('checked', false);
      }
    });
  })(jQuery);
</script>
