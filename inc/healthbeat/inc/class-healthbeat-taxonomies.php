<?php

/**
 * Custom taxonomies for the healthbeat theme
 *
 * @author Elexicon, Inc.
 * @version 1.0.0
 */

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

if( !class_exists('Healthbeat_Taxonomies') ) :

	class Healthbeat_Taxonomies {

		/**
		 * Initializes the custom taxonomies
		 */
		public function __construct() {
			add_action( 'init', 'Healthbeat_Taxonomies::taxonomies', 0 );
		}

		/**
		 * Custom taxonomy registration
		 *
		 * Duplicate the 'register_taxonomy()' function and replace with your own parameters
		 *
		 * @return null
		 */
		public static function taxonomies() {
			// Add new "Blogs" taxonomy to Posts
			register_taxonomy('expert_blog', array('post', 'patient-story', 'inforgraphics', 'videos', 'events'), array(
			    'hierarchical' => true,
			    'labels' => array(
			      	'name' => _x( 'Blogs', 'taxonomy general name' ),
				  	'singular_name' => _x( 'Blog', 'taxonomy singular name' ),
				  	'search_items' =>  __( 'Search Blogs' ),
				  	'all_items' => __( 'All Blogs' ),
				  	'parent_item' => __( 'Parent Blog' ),
				  	'parent_item_colon' => __( 'Parent Blog:' ),
				  	'edit_item' => __( 'Edit Blog' ),
				  	'update_item' => __( 'Update Blog' ),
				  	'add_new_item' => __( 'Add New Blog' ),
				  	'new_item_name' => __( 'New Blog Name' ),
				  	'menu_name' => __( 'Blogs' ),
				),

			    'rewrite' => array(
					'slug' => 'blogs',
					'with_front' => false,
			      'hierarchical' => true
				),
			));


			// Add new "Hospitals" taxonomy to Posts
			register_taxonomy('hospitals', 'post', array(
			    'hierarchical' => true,
			    'labels' => array(
			      	'name' => _x( 'Hospitals', 'taxonomy general name' ),
				  	'singular_name' => _x( 'Hospital', 'taxonomy singular name' ),
				  	'search_items' =>  __( 'Search Hospitals' ),
				  	'all_items' => __( 'All Hospitals' ),
				  	'parent_item' => __( 'Parent Hospital' ),
				  	'parent_item_colon' => __( 'Parent Hospital:' ),
				  	'edit_item' => __( 'Edit Hospital' ),
				  	'update_item' => __( 'Update Hospital' ),
				  	'add_new_item' => __( 'Add New Hospital' ),
				  	'new_item_name' => __( 'New Hospital Name' ),
				  	'menu_name' => __( 'Hospitals' ),
				),

			    'rewrite' => array(
					'slug' => 'hospital',
					'with_front' => false,
			      'hierarchical' => true
				),
			));


			// Add "Secondary Tags" taxonomy to Posts
			register_taxonomy('secondary_tags', 'post', array(
			    'hierarchical' => true,
			    'labels' => array(
			      	'name' => _x( 'Secondary Categories', 'taxonomy general name' ),
				  	'singular_name' => _x( 'Secondary Category', 'taxonomy singular name' ),
				  	'search_items' =>  __( 'Search Secondary Categories' ),
				  	'all_items' => __( 'All Secondary Categories' ),
				  	'parent_item' => __( 'Parent Secondary Category' ),
				  	'parent_item_colon' => __( 'Parent Secondary Category:' ),
				  	'edit_item' => __( 'Edit Secondary Category' ),
				  	'update_item' => __( 'Update Secondary Category' ),
				  	'add_new_item' => __( 'Add New Secondary Category' ),
				  	'new_item_name' => __( 'New Secondary Category' ),
				  	'menu_name' => __( 'Secondary Categories' ),
				),

			    'rewrite' => array(
					'slug' => 'tag',
					'with_front' => false,
			      'hierarchical' => true
				),
			));

			// Add "Series" taxonomy to Posts
			register_taxonomy('special-series', array('post', 'patient-stories', 'videos', 'infographics', 'events', 'promos'), array(
			    'hierarchical' => true,
			    'labels' => array(
			      	'name' => _x( 'Series', 'taxonomy general name' ),
				  	'singular_name' => _x( 'Series', 'taxonomy singular name' ),
				  	'search_items' =>  __( 'Search Series' ),
				  	'all_items' => __( 'All Series' ),
				  	'parent_item' => __( 'Parent Series' ),
				  	'parent_item_colon' => __( 'Parent Series:' ),
				  	'edit_item' => __( 'Edit Series' ),
				  	'update_item' => __( 'Update Series' ),
				  	'add_new_item' => __( 'Add New Series' ),
				  	'new_item_name' => __( 'New Series Name' ),
				  	'menu_name' => __( 'Series' ),
				),

			    'rewrite' => array(
					'slug' => 'series',
					'with_front' => false,
			      	'hierarchical' => true
				),
			));

			// Add "Brand Series" taxonomy to Posts
			register_taxonomy('brand-series', array('post', 'patient-stories', 'videos', 'infographics', 'events', 'promos'), array(
				'hierarchical' => true,
				'labels' => array(
					'name' => _x( 'Brand Series', 'taxonomy general name' ),
					'singular_name' => _x( 'Brand Series', 'taxonomy singular name' ),
					'search_items' =>  __( 'Search Brand Series' ),
					'all_items' => __( 'All Brand Series' ),
					'parent_item' => __( 'Parent Brand Series' ),
					'parent_item_colon' => __( 'Parent Brand Series:' ),
					'edit_item' => __( 'Edit Brand Series' ),
					'update_item' => __( 'Update Brand Series' ),
					'add_new_item' => __( 'Add New Brand Series' ),
					'new_item_name' => __( 'New Brand Series Name' ),
					'menu_name' => __( 'Brand Series' ),
				),

				'rewrite' => array(
					'slug' => 'inspire-innovate-impact',
					'with_front' => false,
					'hierarchical' => true
				),
			));

			flush_rewrite_rules();
		}
	}

	new Healthbeat_Taxonomies();
endif;
