<?php

/**
 * Helper functions
 *
 * @author Elexicon, Inc.
 * @version 1.0.0
 */

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

if( !class_exists('Healthbeat_Helper') ) :

	class Healthbeat_Helper {

		public static $parts;
		public static $icons_url;
		public static $subscribe_url;

		public function __construct() {
			self::$parts = 'template-parts/';
			self::$icons_url = '//icongr.am/';
			self::$subscribe_url = 'http://pages.s4.exacttarget.com/page.aspx?QS=3935619f7de112efd1921ca06e13853faec0e787aaf12b18522c34e76e2a1703';
		}

		/**
		 * Return an image element with an icon from icongr.am
		 * @param  string  $lib   The icon library (clarity, entypo, feather, fontawesome, material, octicons, simple)
		 * @param  string  $icon  The icon name
		 * @param  integer $size  The size of the icon in pixels (default 24)
		 * @param  string  $color Color of the icon to render
		 * @return string         HTML image element
		 */
		public static function icongram_icon($lib, $icon, $size = 24, $color = 'FFFFFF', $echo = true) {
			if($echo) {
				echo '<img src="' . self::$icons_url . $lib . '/' . $icon . '.svg?size=' . $size . '&color=' . $color . '" />';
			} else {
				return '<img src="' . self::$icons_url . $lib . '/' . $icon . '.svg?size=' . $size . '&color=' . $color . '" />';
			}
		}

		/**
		 * Truncate a string of text
		 * @param  string $string The string to truncate
		 * @param  int $limit	How many words are allowed in the string?
		 * @param  string $break  Character to break the string at
		 * @param  string $pad    Characters to place at the end of the string after chopped
		 * @return string        	The truncated string
		 */
		public static function truncate_string($string, $limit, $break=".", $pad="...") {
		  	// return with no change if string is shorter than $limit
		  	if(strlen($string) <= $limit) return $string;

		  	// is $break present between $limit and the end of the string?
		  	if(false !== ($breakpoint = strpos($string, $break, $limit))) {
		    	if($breakpoint < strlen($string) - 1) {
					$string = substr($string, 0, $breakpoint) . $pad;
				}
			}

			return $string;
		}

		/**
		 * Remove first image from content
		 * @param string $the_string - Post content to strip first image from
		 */
		public static function remove_first_image($the_string) {
			$count = substr_count($the_string, '<img');
			if ($count > 0) {
				$length = strlen($the_string);
				$imgBeg = strpos($the_string, '<img');
				$imgEnd = strpos($the_string, '>', $imgBeg);
				$chop = $length-$imgEnd;
				$new_content = substr($the_string, 0, $imgBeg) . substr($the_string, $imgEnd+1, $chop);
				$capcount = substr_count($new_content, '<figcaption');
				if ($capcount > 0) {
					$length = strlen($new_content);
					$capBeg = strpos($new_content, '<figcaption');
					$capClose = strpos($new_content, '</figcaption');
					$capEnd = strpos($new_content, '>', $capClose);
					$chop = $length-$capEnd;
					$final_content = substr($new_content, 0, $capBeg) . substr($new_content, $capEnd+1, $chop);
					return $final_content;
				}
				else {
					return $new_content;
				}
			}
			else {
				return $the_string;
			}
		}

		/**
		 * Get URL to First Embedded Image
		 */
		public static function get_first_image() {
		  	global $post, $posts;
		  	$first_img = '';
		  	ob_start();
		  	ob_end_clean();
		  	$output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);

			// If the 'Social Image' custom field is populated, use that for the OG:Image meta tag
			if(get_field('social_image') && strlen(get_field('social_image')) > 1) {
				$first_img = get_field('social_image');
			} else {
				// If a photo gallery is present, take the first image of that for the OG:Image meta tag
				if(get_field('photo_gallery') == '1') {
					if(have_rows('gallery_images')) {
						$count = 0;
						while(have_rows('gallery_images')) {
							the_row();
							$count++;

							$imgObj = get_sub_field('gallery_image');

							if($count == 1) {
								$first_img = $imgObj['url'];
							}
						}
					}
				} elseif(isset($matches[1][0])) {
					// Else if no gallery or no custom OG:Image is uploaded, use the first image in the post
					$first_img = $matches[1][0];
				} else {
					// If no images are found just use the default SH image
					$first_img = esc_url( get_template_directory_uri() . '/images/placeholder_200x200.jpg' );
				}
			}

		  	return $first_img;
		}

		/**
		 * Get the size (width/height) of the first embedded image
		 */
		public static function get_first_image_size() {
			global $post, $posts;
		  	$first_img = '';
		  	ob_start();
		  	ob_end_clean();

			// Strip the image references from the content
		  	$output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);

			// Strip the image tag
			$image_tag = preg_match_all('/(width|height)=("[^"]*")/i', $matches[0][0], $image);

			// Strip the defined width & height of the image
			$image_w = preg_replace("/[^0-9]/", "", $image[0][0]);
			$image_h = preg_replace("/[^0-9]/", "", $image[0][1]);

			$image_arr = array();
			$image_arr['width'] = $image_w;
			$image_arr['height'] = $image_h;

			return $image_arr;
		}

		/**
		 * Get the Attachment ID for a given image URL.
		 *
		 * @link   http://wordpress.stackexchange.com/a/7094
		 * @param  string $url
		 * @return boolean|integer
		 */
		 public static function get_attachment_id_by_url( $url ) {
			 	$attachment_id = 0;
				$file = basename( $url );
				$query_args = array(
					'post_type'   => 'attachment',
					'post_status' => 'inherit',
					'fields'      => 'ids',
					'meta_query'  => array(
						array(
							'value'   => $file,
							'compare' => 'LIKE',
							'key'     => '_wp_attachment_metadata',
						),
					)
				);
				$query = new WP_Query( $query_args );
				if ( $query->have_posts() ) {
					foreach ( $query->posts as $post_id ) {
						$meta = wp_get_attachment_metadata( $post_id );
						$original_file       = basename( $meta['file'] );
						$cropped_image_files = wp_list_pluck( $meta['sizes'], 'file' );
						if ( $original_file === $file || in_array( $file, $cropped_image_files ) ) {
							$attachment_id = $post_id;
							break;
						}
					}
				}
			 	return $attachment_id;
 		}


		/**
		 * Get archive page data
		 * @return array Array of archive page data (year/month/day)
		 */
		public static function get_archive_data() {
			$archive_data = array();

			$archive_data['title'] = '';

			$archive_data['year'] = get_query_var('year');

			if(is_month()) {
				$month_obj = DateTime::createFromFormat('!m', get_query_var('monthnum'));
				$archive_data['month'] = $month_obj->format('F');

				$archive_data['title'] = $archive_data['title'] . $archive_data['month'] . ' ' . $archive_data['year'];
			} else {
				$archive_data['month'] = '';
			}

			if(is_day()) {
				$day_obj = DateTime::createFromFormat('!d', get_query_var('day'));
				$archive_data['day'] = $day_obj->format('l');

				$month_obj = DateTime::createFromFormat('!m', get_query_var('monthnum'));
				$archive_data['month'] = $month_obj->format('F');

				$archive_data['title'] = $archive_data['title'] . $archive_data['day'] . ', ' . $archive_data['month'] . ' ' . $archive_data['year'];
			} else {
				$archive_data['day'] = '';
			}

			if(is_year()) {
				$archive_data['title'] = $archive_data['title'] . '' . $archive_data['year'];
			}

			return $archive_data;
		}

		/**
		 * Make URL string a clickable link
		 * @param  string $text Link text to parse
		 * @return string       Clickable link markup
		 */
		public static function make_url_link($message) {
			//Convert all urls to links
	    $message = preg_replace('#([\s|^])(www)#i', '$1http://$2', $message);
	    $pattern = '#((http|https|ftp|telnet|news|gopher|file|wais):\/\/[^\s]+)#i';
	    $replacement = '<a href="$1" target="_blank">$1</a>';
	    $message = preg_replace($pattern, $replacement, $message);

	    /* Convert all E-mail matches to appropriate HTML links */
	    $pattern = '#([0-9a-z]([-_.]?[0-9a-z])*@[0-9a-z]([-.]?[0-9a-z])*\\.';
	    $pattern .= '[a-wyz][a-z](fo|g|l|m|mes|o|op|pa|ro|seum|t|u|v|z)?)#i';
	    $replacement = '<a href="mailto:\\1">\\1</a>';
	    $message = preg_replace($pattern, $replacement, $message);
	    return $message;
		}

		/**
		 * Count the number of returned search results
		 * @return string Number of returned results
		 */
		public static function search_results_count() {
		  if( is_search() ) {

		    global $wp_query;

		    if( $wp_query->found_posts == 1 ) {
		      $result_count= '1 Result';
		    } else {
		      $result_count = $wp_query->found_posts.' Results';
		    }

		    return $result_count;

		  }
		}

		/**
		 * Render an SVG image with a PNG fallback
		 * @param  string $img    Name of image to retrieve
		 * @param  int 		$width  Width of the image to render
		 * @param  int 		$height Height of the image to render
		 * @return string        	The SVG image markup w/ PNG fallback
		 */
		public static function render_svg_img($img, $width, $height) {
		?>
			<svg width="<?php echo $width; ?>" height="<?php echo $height; ?>">
				<image xlink:href="<?php echo get_template_directory_uri(); ?>/img/<?php echo $img; ?>.svg" src="<?php echo get_template_directory_uri(); ?>/img/<?php echo $img; ?>.png" width="<?php echo $width; ?>" height="<?php echo $height; ?>" />
			</svg>
		<?php
		}

		/**
		 * Render a standard Health Beat CTA element
		 * @param  array $args 	Array of CTA content/settings
		 * @return string      	HTML markup of HB CTA
		 */
		public static function render_cta($args) {
			if(empty($args) || !is_array($args))
				return;

		?>
			<div class="cta <?php echo $args['type']; ?>">
				<div class="cta-icon">
					<?php Healthbeat_Helper::render_svg_img($args['icon']['name'], $args['icon']['width'], $args['icon']['height']); ?>
				</div>
				<div class="cta-content">
					<h2><?php echo $args['title']; ?></h2>
					<p>
						<?php echo $args['content']; ?>
					</p>
				</div>
				<div class="cta-button">
					<a href="<?php echo $args['button']['url']; ?>" class="btn btn-default btn-green-gradient large"><?php echo $args['button']['text']; ?></a>
				</div>
			</div>
		<?php
		}

		/**
		 * Get the standard header content for each page
		 * @param  int $post_id Post/Page ID
		 * @return string	The content to be rendered at the header
		 */
		public static function get_page_header($post_id = false) {
			if(!$post_id || strlen($post_id) < 1) {
				global $post;
				$post_id = $post->ID;
			}

			remove_filter( 'the_content', 'wpautop' );
			$content = Healthbeat_Posts::get_the_content_with_formatting();

			if(!$content || strlen($content) < 1) {
				$content = self::make_url_link(get_field('page_description', $post_id));
			}

			echo '<p>' . $content . '</p>';
		}
	}

	new Healthbeat_Helper();
endif;
