<?php

/**
 * Health Beat shortcodes
 *
 * @author Elexicon, Inc.
 * @version 1.0.0
 */

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

if( !class_exists('Healthbeat_Shortcodes') ) :

	class Healthbeat_Shortcodes {

		public function __construct() {
      add_shortcode( 'hb_subscribe', array($this, 'hb_subscribe_sc') );
			add_shortcode( 'hb_video_views', array($this, 'hb_video_views_sc') );
			add_shortcode( 'hb_share_list', array($this, 'hb_share_list_sc') );
		}

    public function hb_subscribe_sc($atts) {
      $a = shortcode_atts( array(
        'placeholder' => "Enter your email (we'll take care of the rest)",
				'size' => 'normal'
      ), $atts);

			if($a['size'] === 'normal') {
				$class['input'] = 'col-md-8';
				$class['button'] = 'col-md-4 no-padding';
			} else {
				$class['input'] = 'col-sm-7';
				$class['button'] = 'col-sm-5';
			}

      ob_start();
    ?>
		<form name="subscribe" action="" class="subscribe">
			<div class="input-group">
				<label class="sr-only">Subscribe</label>
				<div class="<?php echo $class['input']; ?> no-padding">
					<input type="email" class="form-control" id="subscribe-email" placeholder="<?php echo $a['placeholder']; ?>" />
				</div>
				<div class="<?php echo $class['button']; ?>">
					<button type="submit" id="submit-subscribe" class="btn btn-default btn-green-gradient large">
						Subscribe
					</button>
				</div>
			</div>
		</form>
    <?php

      return ob_get_clean();
    }

		/**
		 * Return view counts for videos
		 * @param  array $atts array of shortcode data
		 * @return int   number of video views from YouTube
		 */
		public function hb_video_views_sc($atts) {
			$count = 0;
			$data = wp_remote_get("https://www.googleapis.com/youtube/v3/videos?part=statistics&id=" . $atts['id'] . "&key=AIzaSyBIa8i_c_375EeCi5269lsSWjHcpt2zgSc");

			if(is_array($data)) {
				$data = json_decode($data['body']);

				$count = $data->items[0]->statistics->viewCount;
			}

			return number_format($count);
		}

		public function hb_share_list_sc($atts) {
			global $post;

			$a = shortcode_atts(array(
				'title' => 'Did you enjoy this story?',
				'list_title' => 'Share:',
				'post_id' => $post->ID,
				'single' => 'false',
				'fb_like' => 'false'
			), $atts);

			ob_start();
		?>
			<?php if($a['single'] == 'false') : ?>
				<h3><?php echo $a['title']; ?></h3>
			<?php endif; ?>
			<?php
				 $share_url = urlencode(get_the_permalink($a['post_id']));
				 $share_title = urlencode(html_entity_decode(get_the_title($a['post_id']), ENT_COMPAT, 'UTF-8'));
				 $share_excerpt = urlencode(html_entity_decode(get_the_excerpt($a['post_id']), ENT_COMPAT, 'UTF-8'));
			?>
			<ul class="share-list">
				<li class="title"><?php echo $a['list_title']; ?></li>
				<li><a onclick="dataLayer.push({'category': 'share-it', 'action' : 'share', 'label' : 'facebook', 'event': 'share-click'});" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $share_url; ?>" class="facebook"><i class="fa fa-facebook"></i></a></li>
				<li><a onclick="dataLayer.push({'category': 'share-it', 'action' : 'share', 'label' : 'twitter', 'event': 'share-click'});" href="https://twitter.com/share?text=<?php echo $share_title; ?>&url=<?php echo $share_url; ?>&hashtags=SpectrumHealthBeat&via=spectrumhealth" class="twitter"><i class="fa fa-twitter"></i></a></li>
				<li><a onclick="dataLayer.push({'category': 'share-it', 'action' : 'share', 'label' : 'linkedin', 'event': 'share-click'});" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo $share_url; ?>&title=<?php echo $share_title; ?>&summary=<?php echo $share_excerpt; ?>&source=" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
				<li><a onclick="dataLayer.push({'category': 'share-it', 'action' : 'share', 'label' : 'pinterest', 'event': 'share-click'});" href="http://pinterest.com/pin/create/button/?url=<?php echo $share_url; ?>&media=<?php echo get_the_post_thumbnail_url($a['post_id'], 'large'); ?>&description=<?php echo $share_excerpt; ?>" class="pinterest"><i class="fa fa-pinterest"></i></a></li>
				<li><a onclick="dataLayer.push({'category': 'share-it', 'action' : 'share', 'label' : 'google-plus', 'event': 'share-click'});" href="https://plus.google.com/share?url=<?php echo $share_url; ?>" class="google"><i class="fa fa-google-plus"></i></a></li>
				<?php if($a['fb_like'] === 'true') : ?>
					<li>
						<div class="fb-like" data-href="<?php echo $share_url; ?>" data-layout="button" data-action="like" data-size="small" data-show-faces="false" data-share="false"></div>
					</li>
				<?php endif; ?>
			</ul>
		<?php

			return ob_get_clean();
		}
	}

	new Healthbeat_Shortcodes();
endif;
