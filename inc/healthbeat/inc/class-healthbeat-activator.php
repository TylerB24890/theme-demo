<?php
/**
 * Fired during plugin activation
 *
 * @author Elexicon, Inc.
 * @version 1.0.0
 */

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

if(!class_exists('Healthbeat_Activator')) :

	class Healthbeat_Activator {
		public static function activate() {

			if( !function_exists('acf_add_local_field_group') ) {
				echo '<h3>'.__('Please ensure the Advanced Custom Fields Pro plugin is installed and activated.', 'healthbeat').'</h3>';

        		//Adding @ before will prevent XDebug output
        		@trigger_error(__('Please ensure the Advanced Custom Fields Pro plugin is installed and activated.', 'healthbeat'), E_USER_ERROR);
			} else {
				return;
			}

		}
	}

endif;
