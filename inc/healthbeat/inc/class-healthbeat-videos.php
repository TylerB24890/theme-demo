<?php

/**
 * Health Beat Videos
 *
 * Handles all functionality for Videos on Health Beat
 *
 * @author Elexicon, Inc.
 * @version 1.0.0
 */

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

if( !class_exists('Healthbeat_Videos') ) :

	class Healthbeat_Videos {

		/**
		 * Get youtube video ID from URL/Post Content
		 * @param  string $content The content from a WP Post
		 * @return string YouTube Video ID
		 */
		public static function get_video_id($content) {
			if (preg_match('/youtube\.com\/watch\?v=([^\&\?\/]+)/', $content, $id)) {
			  $values = $id[1];
			} else if (preg_match('/youtube\.com\/embed\/([^\&\?\/]+)/', $content, $id)) {
			  $values = $id[1];
			} else if (preg_match('/youtube\.com\/v\/([^\&\?\/]+)/', $content, $id)) {
			  $values = $id[1];
			} else if (preg_match('/youtu\.be\/([^\&\?\/]+)/', $content, $id)) {
			  $values = $id[1];
			} else if (preg_match('/youtube\.com\/verify_age\?next_url=\/watch%3Fv%3D([^\&\?\/]+)/', $content, $id)) {
			  $values = $id[1];
			} else if(preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $content, $id)) {
				$values = $id[1];
			}

			return $values;
		}

    /**
     * Get video data
     * @param  int $pid Post ID to retrieve the video from
     * @return array Array of video data
     */
		public static function get_video_data($pid = false) {
			$video = array();

			if(!$pid)
				return false;

			$post_data = Healthbeat_Posts::get_post_data($pid);

      $video['content'] = $post_data['content'];
      $video['id'] = self::get_video_id($video['content']);
      $video_embed = get_media_embedded_in_content($video['content']);
      $video['embed'] = $video_embed[0];
      $video['thumb'] = get_video_thumbnail($pid);
      $video['category'] = $post_data['category'];
			$video['title'] = $post_data['title'];
			$video['url'] = $post_data['url'];
			$video['date'] = $post_data['date'];
			$video['excerpt'] = $post_data['excerpt'];
			$video['views'] = do_shortcode('[hb_video_views id=' . $video['id'] . ']');

			return $video;
		}

	}

	new Healthbeat_Videos();
endif;
