<?php
/**
 * Health Beat Hospital Widget
 *
 * @package healthbeat
 * @subpackage	healthbeat/inc
 */

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

if(!class_exists('Healthbeat_Hospital_Widget')) :

	class Healthbeat_Hospital_Widget extends WP_Widget {
		public function __construct() {

      $widget_options = array(
        'classname' => 'menu-hospitals-container',
        'description' => 'A select box of Spectrum Health Regional Hospitals',
      );

      parent::__construct( 'regional_hospitals', 'Regional Hospital Select', $widget_options );
		}

    public function widget($args, $instance) {
      $title = apply_filters('widget_title', $instance['title']);

      // Get Regional Hospital Navigation
      $utility_args = array(
        'menu' => 'Hospitals',
        'walker'         => new Walker_Nav_Menu_Dropdown(),
        'items_wrap'     => '<div class="regional-select"><select onchange="if (this.value) window.location.href=this.value">%3$s</select></div>',
      );

      echo '<div class="nav-section">';
      echo '<h2 class="nav-title">' . $title . '</h2>';
      wp_nav_menu($utility_args);

    ?>
      <script>
        jQuery('.regional-select select').on('change', function(e) {
          var url = jQuery(this).val();
          var urlParts = url.split("/");
          var hospitalSlug = urlParts[4];
          dataLayer.push({'category': 'regional-hospitals', 'action' : 'dropdown-select-' + hospitalSlug, 'label' : 'dropdown-select'});
        });
      </script>
    <?php
      echo '</div>';
    }

    public function form( $instance ) {
      $title = ! empty( $instance['title'] ) ? $instance['title'] : ''; ?>
      <p>
        <label for="<?php echo $this->get_field_id( 'title' ); ?>">Title:</label>
        <input type="text" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo esc_attr( $title ); ?>" />
      </p><?php
    }

    public function update( $new_instance, $old_instance ) {
      $instance = $old_instance;
      $instance[ 'title' ] = strip_tags( $new_instance[ 'title' ] );
      return $instance;
    }
	}

	new Healthbeat_Hospital_Widget();

  function hb_regional_hospital_widget() {
    register_widget('Healthbeat_Hospital_Widget');
  }
  add_action( 'widgets_init', 'hb_regional_hospital_widget' );
endif;
