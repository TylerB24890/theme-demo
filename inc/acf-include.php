<?php
/**
 * Includes the Advanced Custom Fields plugin in the theme by default
 */

// Customize ACF Path
add_filter('acf/settings/path', 'healthbeat_acf_settings_path');
function healthbeat_acf_settings_path( $path ) {

    // update path
    $path = get_stylesheet_directory() . '/inc/acf/';

    // return
    return $path;

}


// Customize ACF Directory
add_filter('acf/settings/dir', 'healthbeat_acf_settings_dir');
function healthbeat_acf_settings_dir( $dir ) {

    // update path
    $dir = get_stylesheet_directory_uri() . '/inc/acf/';

    // return
    return $dir;

}

// Include ACF main file
include_once( get_stylesheet_directory() . '/inc/acf/acf.php' );
