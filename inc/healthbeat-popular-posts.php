<?php

/**
 * Custom markup to return from WordPress Popular Posts plugin
 */

 /*
  * Builds custom HTML.
  *
  * With this function, I can alter WPP's HTML output from my theme's functions.php.
  * This way, the modification is permanent even if the plugin gets updated.
  *
  * @param	array	$mostpopular
  * @param	array	$instance
  * @return	string
  */
function hb_popular_posts_html_list( $mostpopular, $instance ) {

  if(!is_single()) {
    $output = '<div class="wpp-list">';

    // loop the array of popular posts objects
    foreach( $mostpopular as $popular ) {

      $thumbnail_url = get_the_post_thumbnail_url($popular->id);
      $post_url = get_the_permalink($popular->id);

      $output .= '<div class="col-md-5ths col-sm-6"><div class="popular-post">';
      $output .= '<div class="popular-post-image">';
      $output .= '<a href="' . $post_url . '"><img src="' . $thumbnail_url . '" /></a>';
      $output .= '</div>';
     	$output .= '<h3 class="entry-title"><a href="' . $post_url . '" title="' . esc_attr( $popular->title ) . '">' . $popular->title . '</a></h3>';
      $output .= '<div class="stats">';
      $output .= '<ul>';
      $output .= '<li>' . Healthbeat_Helper::icongram_icon('clarity', 'share', 18, '497ebb', false) . ' ' . do_shortcode('[share_count_return url="' . $post_url . '" id="' . $popular->id . '"]') . '<span id="' . $popular->id . '" class="shares"></span></li>';
      $output .= '<li>';
      $output .= get_the_date('m/d/y', $popular->id);
      $output .= '</li>';
      $output .= '</div>';
     	$output .= '</div></div>';

    }
    $output .= '</div>';

  } else {

    $output = '<ol class="most-popular-list">';

    // loop the array of popular posts objects
    foreach( $mostpopular as $popular ) {
      $post_url = get_the_permalink($popular->id);
      $output .= '<li><a href="' . $post_url . '" title="' . esc_attr( $popular->title ) . '">' . $popular->title . '</a></li>';
    }

    $output .= '</ol>';
  }

  return $output;
}

add_filter( 'wpp_custom_html', 'hb_popular_posts_html_list', 10, 2 );
