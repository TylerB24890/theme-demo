<?php

/**
 * Miscellaneous Theme Functions
 * @package healthbeat
 * @subpackage healthbeat/inc
 */


/**
 * Set the meta title tag
 * @param  string $title Default title of page
 * @param  string $sep Title separator (i.e. | )
 * @return  string The newly generated page title
 */
function healthbeat_wp_title( $title, $sep ) {
	if ( is_feed() ) {
		return $title;
	}

	global $page, $paged;

	// Add the blog name
	$site_name = "Spectrum Health";

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );

	if(is_single()) {
		$title = single_post_title() . " " . $sep . " Health Beat " . $sep . " " . $site_name;
	} elseif($paged > 1) {
		$title = "Archive " . $sep . " Page " . $paged . " " . $sep .  " Health Beat " . $sep . " " . $site_name;

		if(is_category() || is_tax()) {
			$title = "Archive " . $sep . " " . single_term_title('', false) . " " . $sep . " Page " . $paged . " " . $sep .  " Health Beat " . $sep . " " . $site_name;
		}
	} elseif(is_category() || is_tax()) {
		$title = single_term_title('', false) . " " . $sep . " Health Beat " . $sep . " " . $site_name;
	} else {
		$title = "Health Beat " . $sep . " " . $site_name;
	}

	return $title;
}
add_filter( 'wp_title', 'healthbeat_wp_title', 10, 2 );

/**
 * Disables HTML filters from Category/Taxonomy Descriptions
 */
foreach (array('pre_term_description', 'pre_link_description', 'pre_link_notes', 'pre_user_description') as $filter) {
	remove_filter($filter, 'wp_filter_kses');
}
foreach (array('term_description', 'link_description', 'link_notes', 'user_description') as $filter) {
	remove_filter($filter, 'wp_kses_data');
}

/**
 * Change Default Video Embed Size
 */
function large_video_embed() {
	return array('width' => 960, 'height' => 540);
}
add_filter('embed_defaults', 'large_video_embed');

/**
 * Extend search to include custom fields
 * @param  string $join original search query
 * @return string New search query
 */
function cf_search_join( $join ) {
  global $wpdb;
  if ( is_search() ) {
    $join .=' LEFT JOIN '.$wpdb->postmeta. ' ON '. $wpdb->posts . '.ID = ' . $wpdb->postmeta . '.post_id ';
  }
  return $join;
}
add_filter('posts_join', 'cf_search_join' );

/**
 * Prevent duplicates
 *
 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_distinct
 */
function cf_search_distinct( $where ) {
  global $wpdb;
  if ( is_search() ) {
    return "DISTINCT";
  }
  return $where;
}
add_filter( 'posts_distinct', 'cf_search_distinct' );

// Remove WP Meta Box for Custom Fields
// Improves page load time
add_filter('acf/settings/remove_wp_meta_box', '__return_true');

/**
 *	Add necessary social sharing meta tags to header
 *	@author	Tyler Bailey
 */


// Add Open Graph Meta Info
function insert_social_meta() {
	global $post;

	$global_share_desc = "Your daily dose of inspiration. From informational to motivational, health stories you don’t want to miss! Get inspired. Get informed. spectrumhealthbeat.org";
	$global_share_img = esc_url( get_template_directory_uri() . '/img/placeholder_200x200_v2.jpg' );
	$path = $_SERVER['REQUEST_URI'];

	// If IS single article
	if (is_single()) {

		$title = $post->post_title;
		$type = "article";

		$share_img = Healthbeat_Helper::get_first_image();

   		// Post Excerpt
		if( $post->post_excerpt ) {
			$share_desc = strip_tags(htmlspecialchars(get_the_excerpt(), ENT_QUOTES, 'UTF-8'));
		} else {
			$share_desc = $global_share_desc;
		}

	} else {

		$title = wp_title( '|', false, 'right' );
		$type = "website";

		if(is_tax() || is_category() || is_tag()) {

			$share_desc = strip_tags(htmlspecialchars(term_description(), ENT_QUOTES, 'UTF-8'));
			$share_img = $global_share_img;

		} elseif(is_home()) {

			$share_desc = $global_share_desc;
			$share_img = $global_share_img;

		} else {

			if(is_page_template('page-patientstories.php')) {

				// Patient Stories
				$share_desc = "Ultimately, medicine is about each person’s story. Our patients and their families share some incredible tales about their own health care journeys.";

			} elseif(is_post_type_archive('infographics')) {

				// Infographics
				$share_desc = "You want facts—and fast—but all the numbers in health care can make your head spin. Our infographics offer clarity by turning statistics and trends into visual stories that can help you get in-the-know, at a glance.";
				$share_img = $global_share_img;

			} elseif(is_page_template('page-photo-galleries.php')) {

				// Photo Galleries
				$share_desc = "Photos provide a window to the world. They evoke emotion, understanding and action. Our photo galleries showcase the people, places and things that Health Beat highlights. They are visual stories to be enjoyed.";

			} else {

				if(has_excerpt()) {
					$share_desc = get_the_excerpt();
				} else {
					$share_desc = $global_share_desc;
				}
			}


			if(has_post_thumbnail() && !is_post_type_archive('infographics')) {
				$share_img = get_the_post_thumbnail_url();
			} else {
				$share_img = $global_share_img;
			}
		}
	}
?>
	<!-- Description for search engines -->
	<meta name="description" content="<?php echo $share_desc; ?>" />
	<!-- Open Graph -->
	<meta property="og:title" content="<?php echo $title; ?>"/>
	<meta property="og:description" content="<?php echo $share_desc; ?>" />
   	<meta property="og:type" content="<?php echo $type; ?>"/>
   	<meta property="og:url" content="<?php echo (is_single() ? get_the_permalink() : site_url($path)); ?>"/>
   	<meta property="og:site_name" content="Health Beat | Spectrum Health"/>
   	<meta property="og:image" content="<?php echo $share_img; ?>" />

   	<!-- Schema.org markup for Google+ -->
	<meta itemprop="name" content="<?php echo $title; ?>">
	<meta itemprop="description" content="<?php echo $share_desc; ?>">
	<meta itemprop="image" content="<?php echo $share_img; ?>">

	<!-- Twitter Card data -->
	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:site" content="@SpectrumHealth">
	<meta name="twitter:title" content="<?php echo $title; ?>">
	<meta name="twitter:description" content="<?php echo Healthbeat_Helper::truncate_string($share_desc, 140); ?>">
	<meta name="twitter:creator" content="@SpectrumHealth">
	<meta name="twitter:image:src" content="<?php echo $share_img; ?>">


<?php
}
add_action( 'wp_head', 'insert_social_meta', 5 );
