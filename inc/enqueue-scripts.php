<?php

/**
 * Enqueue all styles and scripts for the healthbeat theme
 *
 * @package	healthbeat
 * @subpackage healthbeat/inc
 */

// Enqueue jQuery
if (!is_admin()) add_action("wp_enqueue_scripts", "healthbeat_jquery_enqueue", 11);
function healthbeat_jquery_enqueue() {
   	wp_deregister_script('jquery'); // Deregister WP default jQuery
   	wp_register_script('jquery', '//code.jquery.com/jquery-2.2.4.min.js', false, null);
   	wp_enqueue_script('jquery'); // Register jQuery
}

// Conditionally load jQuery locally if not able to pull from CDN
add_action( 'wp_head', 'healthbeat_jquery_load' );
function healthbeat_jquery_load() {
?>
	<script>if (!window.jQuery) { document.write('<script src="<?php echo get_template_directory_uri() . '/js/vendor/jquery-2.2.4.min.js'; ?>" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"><\/script>'); }</script>
<?php
}

add_filter('script_loader_tag', 'healthbeat_add_jquery_integrity', 10, 2);
function healthbeat_add_jquery_integrity($tag, $handle) {
    if ($handle != 'jquery')
    return $tag;

    return str_replace(' src', ' integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous" src', $tag);
}


// The main style/script enqueue
function healthbeat_scripts() {

	// Stylesheets
	wp_enqueue_style( 'wp-styles', get_stylesheet_uri() );
  wp_enqueue_style( 'healthbeat-whitney', "//cloud.typography.com/7977332/722406/css/fonts.css" );
	wp_enqueue_style( 'healthbeat-neueue', "//fast.fonts.net/cssapi/cb10258c-aebe-42a8-8705-fad8329d2f73.css" );
	wp_enqueue_style( 'font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css' );
  wp_enqueue_style( 'fancybox', '//cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.css');
  $theme_style = (HEALTHBEAT_DEV ? 'style.css' : 'style.min.css');
  wp_enqueue_style( 'healthbeat-style', get_template_directory_uri() . '/styles/' . $theme_style );

	// Scripts
	wp_enqueue_script( 'bootstrap-js', get_template_directory_uri() . '/js/vendor/bootstrap.min.js', array('jquery'), '3.3.7', true );
  wp_enqueue_script( 'transition-js', get_template_directory_uri() . '/js/vendor/transition.js', array('jquery', 'bootstrap-js'), '3.1.3', true );
  wp_enqueue_script( 'offcanvas-js', get_template_directory_uri() . '/js/vendor/offcanvas.js', array('jquery', 'bootstrap-js', 'transition-js'), '3.1.3', true );
  wp_enqueue_script( 'slick-js', get_template_directory_uri() . '/js/vendor/slick.js', array('jquery'), '2.0.0', true );
  wp_enqueue_script( 'fancybox-js', '//cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js', array('jquery'), '2.0.0', true );

  if(is_page('infographics') || is_post_type_archive('infographics')) {
    wp_enqueue_script( 'images-loaded-js', get_template_directory_uri() . '/js/vendor/imagesLoaded.min.js', array('jquery'), '4.2.1', true );
    wp_enqueue_script( 'masonry-js', get_template_directory_uri() . '/js/vendor/masonry.min.js', array('jquery', 'images-loaded-js'), '4.1.4', true );
  }

  $theme_script = (HEALTHBEAT_DEV ? 'healthbeat.js' : 'healthbeat.min.js');
  wp_enqueue_script( 'healthbeat-js', get_template_directory_uri() . '/js/' . $theme_script, array('jquery', 'slick-js', 'fancybox-js'), '2.0.0', true );

	// IE conditional scripts array
	$conditional_scripts = array(
		'html5shiv' => '//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js',
		'respond' => '//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js'
	);

	// Enqueue conditional scripts
	healthbeat_enqueue_additional_scripts($conditional_scripts);

	// Localize scripts
	healthbeat_localize_script();
}
add_action( 'wp_enqueue_scripts', 'healthbeat_scripts' );


/**
 * Enqueue additional HTML5 scripts
 * @param array $conditional_scripts
 * @return string HTML markup to load the html5 shiv and respond.js files
 */
function healthbeat_enqueue_additional_scripts($conditional_scripts) {
	// Enqueue conditional scripts
	foreach($conditional_scripts as $handle => $src) {
		wp_enqueue_script($handle, $src, array('jquery'), '', false );
	}

	// Apply IE conditional tags to conditional scripts
	add_filter('script_loader_tag', function($tag, $handle) use ($conditional_scripts) {
		if(array_key_exists($handle, $conditional_scripts)) {
			$tag = "<!--[if lt IE 9]>$tag<![endif]-->";
		}
		return $tag;
	}, 10, 2);
}

/**
 * Localize Javascript variables
 * @link https://codex.wordpress.org/Function_Reference/wp_localize_script
 */
function healthbeat_localize_script() {
    // Localize the global admin-ajax URL
    // usage: healthbeat.ajaxurl;

    global $post;

    $curPage = $post->post_name;

    if(is_archive()) {
      $curPage = get_post_type();
    }

    wp_localize_script(
        'healthbeat-js',
        'healthbeat',
        array(
            'ajaxurl' => admin_url( 'admin-ajax.php' ),
            'isMobile' => (wp_is_mobile() ? true : false),
            'curPage' => $curPage,
            'isHome' => (is_home() || is_front_page() ? true : false),
            'isSingle' => (is_single() ? true : false),
        )
    );
}
?>
