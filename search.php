<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package healthbeat
 */

get_header();

?>

<div id="archive" class="container-fluid">
	<div class="col-sm-9 no-right-padding">

		<div class="col-md-12">
			<div class="archive-header">
				<h1><?php _e('Search Results For:', 'healthbeat'); ?></h1>
				<h2>
					<?php echo get_search_query(); ?>
				</h2>
			</div>

			<?php

			if(have_posts()) :
				while(have_posts()) {
					the_post();
					include(locate_template(Healthbeat_Helper::$parts . 'components/post-list.php'));
				}

				healthbeat_pagination();
			else :
			?>
				<h3><?php _e('No results found.', 'healthbeat'); ?></h3>
		<?php endif; ?>
		</div>
	</div>
	<div class="col-sm-3">
		<?php get_sidebar('main-sidebar'); ?>
	</div>
</div><!-- .container -->

<?php
get_footer();
