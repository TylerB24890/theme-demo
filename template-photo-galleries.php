<?php
/**
 * Template Name: Photo Galleries
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package healthbeat
 */

get_header();

?>

<div id="photo-galleries" class="container-fluid page">
	<div class="col-sm-9 no-right-padding">

		<?php get_template_part(Healthbeat_Helper::$parts . 'components/page', 'header'); ?>

    <div class="clear clearfix"></div>

    <?php

      $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

      $args = array(
        'posts_per_page' => 12,
        'paged' => $paged,
        'post_type' => 'post',
        'meta_key' => 'photo_gallery',
        'meta_value' => '1'
      );

      $temp = $wp_query;
      $wp_query = null;

      $wp_query = new WP_Query($args);

      if ( $wp_query->have_posts() ) :
    ?>
        <div class="posts">
          <?php
            while( $wp_query->have_posts() ) {
              $wp_query->the_post();
              include(locate_template(Healthbeat_Helper::$parts . 'components/post-list.php'));
            }

            healthbeat_pagination();
          ?>
        </div>
    <?php

      endif;

      $wp_query = null;
      $wp_query = $temp;

      wp_reset_postdata();
    ?>
	</div>
	<div class="col-sm-3">
		<?php get_sidebar('main-sidebar'); ?>
	</div>
</div><!-- .container -->

<?php
get_footer();
