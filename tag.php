<?php
/**
 * The template for displaying Tag archives
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package healthbeat
 */

get_header();

$archive = Healthbeat_Helper::get_archive_data();

?>

<div id="archive" class="container-fluid">
	<div class="col-sm-9 no-right-padding">

		<div class="col-md-12">
			<div class="archive-header">
				<h1><?php _e('Tag:', 'healthbeat'); ?></h1>
				<h2>
					<?php single_tag_title(); ?>
				</h2>
			</div>
		</div>

			<?php

			if(have_posts()) {
				while(have_posts()) {
					the_post();
					include(locate_template(Healthbeat_Helper::$parts . 'components/post-list.php'));
				}

				healthbeat_pagination();
			}

			?>

	</div>
	<div class="col-sm-3">
		<?php get_sidebar('main-sidebar'); ?>
	</div>
</div><!-- .container -->

<?php
get_footer();
