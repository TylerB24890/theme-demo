

<?php
   /**
   * The header for our theme.
   *
   * This is the template that displays all of the <head> section and everything up until <div id="content">
   *
   * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
   *
   * @package healthbeat
   */

   ?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
   <head>
      <title><?php wp_title( '|', true, 'right' ); ?></title>
      <meta charset="<?php bloginfo( 'charset' ); ?>">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="profile" href="http://gmpg.org/xfn/11">
      <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
      <link rel="shotcut icon" href="<?php echo get_template_directory_uri() . '/favicon.ico'; ?>">

      <?php wp_head(); ?>
      <script type="text/javascript">
         var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
      </script>
   </head>
   <body <?php body_class(); ?>>

      <?php get_template_part(Healthbeat_Helper::$parts . 'components/offcanvas', 'nav'); ?>

      <div id="main-canvas">
      <header>
         <nav class="navbar navbar-default">
            <div class="backdrop"></div>
            <div class="container-fluid">
               <div class="col-lg-7 col-sm-6 col-xs-12 no-left-padding">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="offcanvas" data-target="#offcanvas-nav" aria-expanded="false" aria-controls="navbar">
                  <span class="sr-only"><?php _e('Toggle Navigation', 'healthbeat'); ?></span>
                  <span class="icon-bar top-bar"></span>
                  <span class="icon-bar middle-bar"></span>
                  <span class="icon-bar bottom-bar"></span>
                  </button>
                  <div class="navbar-header">
                     <a class="navbar-brand sh" href="<?php echo home_url(); ?>">
                        <?php Healthbeat_Helper::render_svg_img('sh-logo', 200, 45); ?>
                     </a>
                     <a class="navbar-brand hb" href="<?php echo home_url(); ?>">
                        <?php Healthbeat_Helper::render_svg_img('healthbeat-logo', 245, 43); ?>
                     </a>
                  </div>
               </div>
               <div class="col-lg-2 col-sm-2 col-xs-6 no-padding">
                  <div class="header-utilities">
                     <?php echo do_shortcode('[google-translator]'); ?>
                  </div>
               </div>
               <div class="col-lg-3 col-sm-4 col-xs-6 no-padding">
                  <div class="header-utilities">
                     <ul class="header-social">
                        <li><a href="http://www.facebook.com/spectrumhealthbeat" id="facebook"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="http://twitter.com/SHHealthBeat" id="twitter"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="https://www.youtube.com/c/spectrumhealthbeatorg" id="youtube"><i class="fa fa-youtube"></i></a></li>
                        <li><a href="https://www.pinterest.com/spectrumhealth/health-beat/" id="pinterest"><i class="fa fa-pinterest"></i></a></li>
                        <li><a href="https://www.instagram.com/health.beat/" id="instagram"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="http://www.spectrumhealth.org"><?php _e('Visit spectrumhealth.org', 'healthbeat'); ?></a></li>
                     </ul>
                  </div>
                  <div class="header-utilities">
                     <div class="clear clearfix"></div>
                     <?php get_search_form(); ?>
                  </div>
               </div>
            </div>
         </nav>
      </header>
      <div id="content" class="site-content">
