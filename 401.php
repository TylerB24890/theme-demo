<?php
/**
 * The template for displaying the 404 Error Page
 *
 * @package healthbeat
 */

get_header(); ?>

<div id="error-401">
	<div class="container-fluid page">
		<div class="col-sm-9 no-right-padding">

			<div class="col-md-12">
        <div class="page-header-container">
          <h1><?php _e('Gone, Error 401', 'healthbeat'); ?></h1>

					<h2>
						<?php _e('Sorry, the content you are looking for has been removed.', 'healthbeat'); ?>
					</h2>

					<div class="col-md-11 no-left-padding">
						<p>
							<?php _e('The page you are looking for no longer exists. You can return back to the sites homepage and see if you can find what you are looking for. Or, you can try a search.', 'healthbeat'); ?>
						</p>
					</div>
        </div>

				<?php get_search_form(); ?>

				<div class="tag-cloud">
					<h3><?php _e('Tags', 'healthbeat'); ?></h3>
					<?php wp_tag_cloud(); ?>
				</div>
      </div>

	    <div class="clear clearfix"></div>
		</div>
		<div class="col-sm-3">
			<?php get_sidebar('main-sidebar'); ?>
		</div>
	</div><!-- .container -->
</div>

<?php
get_footer();
