
Theme Demo
===

A stripped down version of an existing theme in production.

**NOTE:** The theme will spin up in WP, but there are a few plugins and other items missing that may cause some errors throughout some of the templates.

This theme was setup for **code review purposes only** and is not intended to be used else where or for any other reason.
