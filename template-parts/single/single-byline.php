<div class="post-byline">
  <div class="col-md-9 no-left-padding">
    <ul>
      <li class="author">
        <span class="author-img">
          <a href="<?php echo $post_data['author']['url']; ?>">
            <?php echo $post_data['author']['image']; ?>
          </a>
        </span>
        <span class="author-name">
          By: <a href="<?php echo $post_data['author']['url']; ?>"><?php echo $post_data['author']['name']; ?></a>
          <?php $author_name_arr = explode(' ', $post_data['author']['name']); ?>
          <small><a href="<?php echo $post_data['author']['url']; ?>"><?php _e('More articles by', 'healthbeat'); ?> <?php echo $author_name_arr[0]; ?></a></small>
        </span>
      </li>
      <li class="divider"></li>
      <li class="date">
        <?php the_date('F j, Y'); ?>
      </li>
      <li class="divider"></li>
      <li class="comment-shares">
        <span class="meta-comments"><?php comments_number( '0 ' . __('comments', 'healthbeat'), '1 ' . __('comment', 'healthbeat'), '% ' . __('comments', 'healthbeat') ); ?></span>
        <span class="meta-shares"><?php echo do_shortcode('[share_count text=true url="' . $post_data['url'] . '" id="' . $post_data['id'] . '"]'); ?></span>
      </li>

    </ul>
  </div>
  <div class="col-md-3 no-padding">
    <?php echo do_shortcode('[hb_share_list single="true" list_title="' . __('Share', 'healthbeat') . '" fb_like="true" post_id="' . $post->ID . '"]'); ?>
  </div>

  <div class="clear clearfix"></div>

</div>
