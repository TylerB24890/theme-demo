<?php if(have_rows('gallery_images')) : ?>
	<div class="post-gallery">
		<?php
			while(have_rows('gallery_images')) :
				the_row();

				$gallery_image = get_sub_field('gallery_image');
				$image_url = $gallery_image['url'];
				$image_title = $gallery_image['title'];
				$image_caption = $gallery_image['caption'];
				$count++;
		?>
				<div class="gallery-item">
					<a href="<?php echo $image_url ?>" data-fancybox="gallery" data-caption="<?php echo $image_caption; ?>"><img src="<?php echo $image_url ?>" alt="<?php echo $image_title ?>" title="<?php echo sanitize_title_with_dashes($image_title); ?>" /></a>

					<div class="gallery-caption">
						<?php echo $image_caption; ?>
					</div>
				</div>
		<?php endwhile; ?>
	</div>

	<div class="post-gallery-thumbs">
		<?php
			$thumb_count = 0;
			while(have_rows('gallery_images')) :
				the_row();

				$gallery_image = get_sub_field('gallery_image');
				$image_thumb = $gallery_image['sizes']['thumbnail'];
				$image_title = $gallery_image['title'];
				$thumb_count++;
		?>
				<div class="gallery-thumb">
					<img src="<?php echo $image_thumb ?>" alt="<?php echo $image_title ?>" title="<?php echo sanitize_title_with_dashes($image_title); ?>" />
				</div>
		<?php endwhile; ?>
	</div>
<?php endif; ?>
