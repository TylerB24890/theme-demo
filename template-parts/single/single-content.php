<div class="post-content">
  <?php if(get_post_type() === 'infographics') : ?>
    <div class="full-infographic">
      <?php the_post_thumbnail('full'); ?>
    </div>
  <?php
  endif;

  echo Healthbeat_Posts::get_the_content_with_formatting();
  ?>
</div>
