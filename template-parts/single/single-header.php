<a href="<?php echo $post_data['category']['url']; ?>" class="category-flag <?php echo $post_data['category']['slug']; ?>">
	<?php echo $post_data['category']['name']; ?>
</a>

<div class="post-header">
	<h1 class="post-title"><?php echo $post_data['title']; ?></h1>
	<h3 class="post-subhead"><?php the_excerpt(); ?></h3>
</div>
