<?php

if(has_post_thumbnail()) :
  $pc++;

  $post_data = Healthbeat_Posts::get_post_data($post->ID);
?>

  <div class="col-md-4 grid-item">
  	<a class="post-block infographic-block" href="<?php echo $post_data['url']; ?>">
      <div class="category-flag <?php echo $post_data['category']['slug']; ?>">
        <?php echo $post_data['category']['name']; ?>
      </div>
      <div class="infographic-content">
        <div class="post-image">
    			<?php the_post_thumbnail('infographic-block'); ?>
    		</div>
    		<div class="post-content">
          <h3><?php echo $post_data['title']; ?> <!--<img src="//icongr.am/feather/arrow-right.svg?size=24&color=4B6084" />--></h3>
    		</div>
      </div>
  	</a>
  </div>
<?php endif; ?>
