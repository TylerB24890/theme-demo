<?php

  // Get Regional Hospital Navigation
  $utility_args = array(
    'menu' => 'Hospitals',
    'walker'         => new Walker_Nav_Menu_Dropdown(),
    'items_wrap'     => '<div class="regional-select"><select onchange="if (this.value) window.location.href=this.value">%3$s</select></div>',
  );

  wp_nav_menu($utility_args);

?>
<script>
  jQuery('.regional-select select').on('change', function(e) {
    var url = jQuery(this).val();
    var urlParts = url.split("/");
    var hospitalSlug = urlParts[4];
    dataLayer.push({'category': 'regional-hospitals', 'action' : 'dropdown-select-' + hospitalSlug, 'label' : 'dropdown-select'});
  });
</script>
