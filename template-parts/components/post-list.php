<?php
if(has_post_thumbnail()) :
	$pc++;

	$post_data = Healthbeat_Posts::get_post_data($post->ID);

	if(isset($current_cat)) {
		$post_data['category']['slug'] = $cat_slug;
	}
?>

<div class="col-md-12">
	<div class="post-list media <?php echo $post_data['category']['slug']; ?>">
    <div class="media-left">
      <div class="post-image">
				<?php if(!isset($current_cat)) : ?>
	  			<a class="category-flag <?php echo $post_data['category']['slug']; ?>" href="<?php echo $post_data['category']['url']; ?>">
	  				<?php echo $post_data['category']['name']; ?>
	  			</a>
				<?php endif; ?>
        <a href="<?php echo $post_data['url']; ?>">
  			  <?php the_post_thumbnail('post-block', array('class' => 'center-image')); ?>
        </a>
  		</div>
    </div>
    <div class="media-body">
      <div class="post-content">
  			<?php if($post_data['block_type'] !== '') : ?>
  				<div class="block-type <?php echo $post_data['category']['slug'] . '-text'; ?>"><?php echo $post_data['block_type']; ?></div>
  			<?php endif; ?>

  			<a href="<?php echo $post_data['url']; ?>" class="post-title"><h3><?php echo $post_data['title']; ?></h3></a>
  			<p><?php echo $post_data['excerpt']; ?></p>

  			<div class="post-meta">
  				<div class="meta">
  					<ul>
              <li class="author-img">
                <a href="<?php echo $post_data['author']['url']; ?>">
                  <?php echo $post_data['author']['image']; ?>
                </a>
              </li>
              <li class="author-name">
                <?php _x('By:', 'healthbeat', 'Post Author By-line'); ?> <a href="<?php echo $post_data['author']['url']; ?>"><?php echo $post_data['author']['name']; ?></a>
              </li>
              <li class="post-date">
                <?php echo get_the_date('F j, Y', $post->ID); ?>
              </li>
  						<li class="post-shares">
  							<?php Healthbeat_Helper::icongram_icon('clarity', 'share', 22, '497ebb'); ?> <?php echo do_shortcode('[share_count url="' . $post_data['url'] . '" id="' . $post->ID . '"]'); ?>
  						</li>
              <li class="post-comments">
  							<a href="<?php comments_link(); ?>"><i class="fa fa-comment-o"></i> <?php comments_number( '0', '1', '%' ); ?></a>
  						</li>
  					</ul>
  				</div>
  			</div>
  		</div>
    </div>
	</div>
</div>
<?php endif; ?>
