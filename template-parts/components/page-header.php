<?php
  if(have_posts()) :
    while(have_posts()) :
      the_post();
?>
      <div class="col-md-12">
        <div class="page-header-container">
          <h1><?php the_title(); ?></h1>

          <?php Healthbeat_Helper::get_page_header(); ?>

          <?php
            if(is_page('patientstories') || is_page(12395)) {
              $cta_args = array(
  							'type' => 'cta-inline',
  							'icon' => array(
  								'name' => 'share-your-story-icon',
  								'width' => 116,
  								'height' => 105
  							),
  							'title' => __('Share your story', 'healthbeat'),
  							'content' => __('Do you have a story you\'d like to share about your experience as a patient at Spectrum Health?', 'healthbeat'),
  							'button' => array(
  								'url' => '/contact-us/share-your-story/',
  								'text' => __('Get started', 'healthbeat')
  							)
  						);

  						Healthbeat_Helper::render_cta($cta_args);

            }
          ?>
        </div>
      </div>
  <?php endwhile; ?>
<?php endif; ?>
