<?php
$author_url = get_author_posts_url($author->ID);
$author_role = get_field('healthbeat_role', 'user_' . $author->ID);
?>

<div class="block">
  <div class="block-img">
    <?php echo get_avatar($author->ID, 512); ?>
  </div>
  <div class="block-content">
    <a href="<?php echo $author_url; ?>" class="block-title"><?php echo $author->display_name; ?></a>

    <?php if($author_role && strlen($author_role) > 1) : ?>
      <span class="author-role"><?php echo $author_role; ?></span>
    <?php endif; ?>

    <a class="btn-outline" href="<?php echo $author_url; ?>"><?php _e('View stories', 'healthbeat'); ?></a>
  </div>
</div>
