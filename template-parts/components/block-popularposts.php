<?php if(function_exists('wpp_get_mostpopular')) : ?>
<section id="popular-posts">
  <div class="container-fluid">
    <div class="col-md-12">
      <h2 class="section-title"><?php _e('Popular Posts', 'healthbeat'); ?></h2>
    </div>

    <?php
      $args = array(
        'limit' => 10,
        'range' => 'monthly',
        'freshness' => 1,
        'post_type' => 'post,infographics,patient-stories,videos',
        'pid' => '4250,3013'
      );

      wpp_get_mostpopular($args);
    ?>
  </div>
</section>
<?php endif; ?>
