<div id="offcanvas-nav" class="navmenu navmenu-default navmenu-fixed-left offcanvas">
   <div class="navbar-collapse">
      <div class="offcanvas-logo">
         <h2><?php _e('Explore Healthbeat:', 'healthbeat'); ?></h2>
         <div class="header-utilities">
            <div class="clear clearfix"></div>
            <?php get_search_form(); ?>

            <?php if(!is_home() && !is_front_page()) : ?>
              <a class="btn-outline"  href="<?php echo home_url(); ?>" target="_self"><?php _e('Home', 'healthbeat'); ?></a>
            <?php endif; ?>
         </div>
      </div>
      <?php dynamic_sidebar('offcanvas'); ?>
   </div>
</div>
