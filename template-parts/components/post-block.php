<?php
if(has_post_thumbnail()) :
	$pc++;

	$post_data = Healthbeat_Posts::get_post_data($post->ID);

	if(!isset($class) || $class === null) {
		$class = 'col-md-4';
	}
?>

<div class="<?php echo $class; ?> col-sm-6">
	<a class="post-block" href="<?php echo $post_data['url']; ?>">
		<div class="post-image">
			<div class="category-flag <?php echo $post_data['category']['slug']; ?>">
				<?php echo $post_data['category']['name']; ?>
			</div>
			<?php the_post_thumbnail('post-block'); ?>
		</div>
		<div class="post-content">
			<?php if($post_data['block_type'] !== '') : ?>
				<div class="block-type <?php echo $post_data['category']['slug'] . '-text'; ?>"><?php echo $post_data['block_type']; ?></div>
			<?php endif; ?>

			<h3 class="post-title"><?php echo $post_data['title']; ?></h3>
			<p><?php echo $post_data['excerpt']; ?></p>

			<div class="post-meta">
				<div class="byline">
					<span class="author-img"><?php echo $post_data['author']['image']; ?></span>
					<span class="author-name"><?php _x('By:', 'healthbeat', 'Post Author By-line'); ?> <?php echo $post_data['author']['name']; ?></span>
				</div>

				<div class="meta">
					<ul>
						<li>
							<i class="fa fa-comment-o"></i> <?php comments_number( '0', '1', '%' ); ?>
						</li>
						<li>
							<?php Healthbeat_Helper::icongram_icon('clarity', 'share', 22, '497ebb'); ?> <?php echo do_shortcode('[share_count url="' . $post_data['url'] . '" id="' . $post->ID . '"]'); ?>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</a>
</div>
	<?php if($class === 'col-md-4' && $pc % 3 == 0) : ?>
		<div class="clear clearfix"></div>
	<?php elseif($class === 'col-md-3' && $pc % 4 === 0) : ?>
		<div class="clear clearfix"></div>
	<?php endif; ?>
<?php endif; ?>
