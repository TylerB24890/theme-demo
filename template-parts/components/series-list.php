<div class="col-md-12">
	<div id="<?php echo $series->slug; ?>" class="post-list media series">
    <div class="media-left">
      <div class="post-image">
        <a href="<?php echo $series_url; ?>">
  			  <img src="<?php echo $series_image; ?>" alt="<?php echo $series->name; ?>" title="<?php echo $series->slug; ?>" />
        </a>
  		</div>
    </div>
    <div class="media-body">
      <div class="post-content">

  			<a href="<?php echo $series_url; ?>" class="post-title"><h3><?php echo $series->name; ?></h3></a>
  			<p>
          <?php echo $series_excerpt; ?>
        </p>

        <div class="series-meta">
          <span class="series-count"><?php echo $series->count; ?> <?php _e('Stories', 'healthbeat'); ?></span>
          <a class="btn-outline" href="<?php echo $series_url; ?>"><?php _e('View stories', 'healthbeat'); ?></a>
        </div>
  		</div>
    </div>
	</div>
</div>
