<ul id="utility-links" class="no-border" style="margin-top: 0;">
	<li><a href="https://www.spectrumhealth.org/about-us/contact-us/contact-us-multiple-languages" target="_blank">አማርኛ </a></li>
	<li><a href="https://www.spectrumhealth.org/about-us/contact-us/contact-us-multiple-languages" target="_blank">العربية </a></li>
	<li><a href="https://www.spectrumhealth.org/about-us/contact-us/contact-us-multiple-languages" target="_blank">Ako govorite srpsko </a></li>
	<li><a href="https://www.spectrumhealth.org/about-us/contact-us/contact-us-multiple-languages" target="_blank">中文</a></li>
	<li><a href="https://www.spectrumhealth.org/about-us/contact-us/contact-us-multiple-languages" target="_blank">Farsi </a></li>
	<li><a href="https://www.spectrumhealth.org/about-us/contact-us/contact-us-multiple-languages" target="_blank">فارسی </a></li>
	<li><a href="https://www.spectrumhealth.org/about-us/contact-us/contact-us-multiple-languages" target="_blank">Français</a></li>
	<li><a href="https://www.spectrumhealth.org/about-us/contact-us/contact-us-multiple-languages" target="_blank">Kreyòl Ayisyen</a></li>
	<li><a href="https://www.spectrumhealth.org/about-us/contact-us/contact-us-multiple-languages" target="_blank">Ikinyarwanda </a></li>
	<li><a href="https://www.spectrumhealth.org/about-us/contact-us/contact-us-multiple-languages" target="_blank">नेपाली </a></li>
	<li><a href="https://www.spectrumhealth.org/about-us/contact-us/contact-us-multiple-languages" target="_blank">Thuɔŋjaŋ</a></li>
	<li><a href="https://www.spectrumhealth.org/about-us/contact-us/contact-us-multiple-languages" target="_blank">Soomaali </a></li>
	<li><a href="https://www.spectrumhealth.org/about-us/contact-us/contact-us-multiple-languages" target="_blank">Español</a></li>
	<li><a href="https://www.spectrumhealth.org/about-us/contact-us/contact-us-multiple-languages" target="_blank">Sudanese</a></li>
	<li><a href="https://www.spectrumhealth.org/about-us/contact-us/contact-us-multiple-languages" target="_blank">Kiswahili </a></li>
	<li><a href="https://www.spectrumhealth.org/about-us/contact-us/contact-us-multiple-languages" target="_blank">தமிழ </a></li>
	<li><a href="https://www.spectrumhealth.org/about-us/contact-us/contact-us-multiple-languages" target="_blank">ትግርኛ </a></li>
	<li><a href="https://www.spectrumhealth.org/about-us/contact-us/contact-us-multiple-languages" target="_blank">Tiếng Việt </a></li>
</ul>
