<?php

$args = array(
  'post_type' => 'infographics',
  'posts_per_page' => 10
);

$query = new WP_Query($args);

if($query->have_posts()) :
  $ic = 0;
?>
  <section id="infographics" style="padding-bottom: 0;" class="light-gray">
    <div class="container-fluid">
      <div class="col-md-12">
        <div class="section-header" style="position: relative; z-index: 999;">
          <h2 class="section-title"><?php _e('Infographics', 'healthbeat'); ?></h2>
          <a href="/infographics/" class="btn-outline"><?php _e('View all infographics', 'healthbeat'); ?></a>
        </div>
      </div>
    </div>

    <div class="multi-item-carousel" id="infographic-carousel">
      <?php
        while($query->have_posts()) :
          $ic++;
          $query->the_post();

          $thumb = get_field('infographic_thumbnail');
      ?>
        <div class="infographic-item">
          <a href="<?php the_permalink(); ?>">
            <?php the_post_thumbnail('infographic-carousel'); ?>
            <div class="infographic-gradient"></div>
          </a>
        </div>
      <?php endwhile; ?>
    </div>
    <div class="infographic-gradient"></div>
  </section>
<?php endif; wp_reset_postdata(); ?>
