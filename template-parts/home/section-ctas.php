<?php if(have_rows('image_cta_row')) : ?>
  <section id="cta-container" class="cta-row large">
    <?php while(have_rows('image_cta_row')) : the_row(); ?>
      <div class="col-md-4 no-padding">
        <div class="cta" style="overflow: auto; background: url('<?php the_sub_field('cta_image'); ?>') no-repeat center center">
          <div class="cta-content">
            <h2><?php the_sub_field('cta_title'); ?></h2>

            <div class="cta-excerpt">
              <p>
                <?php the_sub_field('cta_content'); ?>
              </p>

              <a class="cta-link-text btn-outline" href="<?php the_sub_field('cta_link'); ?>"><?php the_sub_field('cta_link_text'); ?></a>
            </div>
          </div>
          <!--<div class="cta-gradient"></div>-->
        </div>
      </div>
    <?php endwhile; ?>
  </section>
<?php endif; ?>
