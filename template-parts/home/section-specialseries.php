<?php if(have_rows('special_series')) : ?>
<section id="special-series">
  <div class="container-fluid">
    <div class="col-md-12">
      <div class="section-header">
        <h2 class="section-title"><?php _e('Special Series', 'healthbeat'); ?></h2>
        <a href="/series/" class="btn-outline"><?php _e('View all special series', 'healthbeat'); ?></a>
      </div>
    </div>
    <?php while(have_rows('special_series')) : the_row(); ?>
      <?php
        $series_id = get_sub_field('series');
        $series = get_term_by('id', $series_id, 'special-series');
        $acf_series = 'special-series_' . $series_id;

        $series_img = get_sub_field('custom_series_image');

        if(!$series_img) {
          $series_img = get_field('series_image', $acf_series);
        }

        $series_excerpt = get_field('series_excerpt', $acf_series);
      ?>
      <div class="col-sm-4">
        <div class="series">
          <div class="series-img">
            <a href="/series/<?php echo $series->slug; ?>/">
              <img src="<?php echo $series_img; ?>" alt="<?php echo $series->name; ?>" title="<?php echo $series->slug; ?>" />
            </a>
          </div>
          <a href="/series/<?php echo $series->slug; ?>/"><h3 class="series-title"><?php echo $series->name; ?></h3></a>
          <div class="series-excerpt">
            <p>
              <?php echo $series_excerpt; ?>
            </p>
          </div>
          <a href="/series/<?php echo $series->slug; ?>/" class="btn-outline"><?php _e('Explore this series', 'healthbeat'); ?></a>
        </div>
      </div>
    <?php endwhile; ?>
  </div>
</section>
<?php endif; ?>
