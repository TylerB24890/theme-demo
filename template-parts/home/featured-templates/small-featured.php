<?php

$smpid = get_sub_field('small_feature_post');
$img = get_sub_field('small_feature_image');
$exclude[] = $smpid;

if(!$img) {
  $img = get_the_post_thumbnail_url($smpid, 'sm-featured-4');
} else {
  $img = wp_get_attachment_image_src($img, 'sm-featured-4');
  $img = $img[0];
}

$post_title = get_the_title($smpid);
?>

<div class="col-sm-6 no-padding">
  <div class="sm-featured" style="background: url('<?php echo $img; ?>') no-repeat center center; background-size: cover;">
      <a href="<?php echo get_the_permalink($smpid); ?>">
        <div class="feature-meta">
          <h2><?php echo $post_title; ?></h2>
          <div class="feature-meta-line">
            <span class="meta-comments"><i class="fa fa-comment-o"></i><?php comments_number( '0', '1', '%' ); ?></span>
            <span class="meta-shares"><?php Healthbeat_Helper::icongram_icon('clarity', 'share', 24, '5489c4'); ?><?php echo do_shortcode('[share_count url="' . get_the_permalink($smpid) . '" id="' . $smpid . '"]'); ?></span>
          </div>
        </div>
        <div class="image-gradient"></div>
      </a>
  </div>
</div>
