<?php

$lpid = get_sub_field('large_feature_post');
$img = get_sub_field('large_feature_image');
$exclude[] = $lpid;

if(!$img) {
  $img = get_the_post_thumbnail_url($lpid, 'lg-featured-main');
} else {
  $img = wp_get_attachment_image_src($img, 'lg-featured-main');
  $img = $img[0];
}

$post_data = Healthbeat_Posts::get_post_data($lpid);
$post_title = get_sub_field('large_feature_title');
$post_excerpt = get_sub_field('large_feature_excerpt');

if(!$post_title || strlen($post_title) < 1) {
  $post_title = $post_data['title'];
}

if(!$post_excerpt || strlen($post_excerpt) < 1) {
  $post_excerpt = $post_data['excerpt'];
}

?>

<div class="<?php echo($layout !== 'one_featured' ? 'col-sm-7' : 'col-md-12'); ?> no-padding">
  <div class="lg-featured" style="background: url('<?php echo $img; ?>') no-repeat center center; background-size: cover;">
    <a href="<?php echo $post_data['url']; ?>">
      <div class="feature-meta">
        <?php echo($layout == 'one_featured' ? '<div class="container-fluid"><div class="col-md-12">' : ''); ?>
        <h2><?php echo $post_title; ?></h2>
        <p>
          <?php echo $post_excerpt; ?>
        </p>
        <div class="feature-meta-line">
          <span class="date"><?php echo $post_data['date']; ?></span>

          <div class="author">
  					<span class="author-img"><?php echo $post_data['author']['image']; ?></span>
  					<span class="author-name">By: <?php echo $post_data['author']['name']; ?></span>
          </div>

          <span class="meta-comments"><i class="fa fa-comment-o"></i><?php comments_number( '0', '1', '%' ); ?></span>
          <span class="meta-shares"><?php Healthbeat_Helper::icongram_icon('clarity', 'share', 24, '5489c4'); ?><?php echo do_shortcode('[share_count url="' . $post_data['url'] . '" id="' . $lpid . '"]'); ?></span>
          <?php echo($layout == 'one_featured' ? '</div></div>' : ''); ?>
        </div>
      </div>
      <div class="image-gradient"></div>
    </a>
  </div>
</div>
