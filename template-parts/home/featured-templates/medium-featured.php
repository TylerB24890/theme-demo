<?php

$mpid = get_sub_field('medium_feature_post');
$img = get_sub_field('medium_feature_image');
$post_title = get_sub_field('medium_feature_title');
//$excerpt = get_sub_field('medium_feature_excerpt');
$exclude[] = $mpid;

if(!$img) {
  $img = get_the_post_thumbnail_url($mpid, 'md-featured-4');
} else {
  $img = wp_get_attachment_image_src($img, 'md-featured-4');
  $img = $img[0];
}

if(!$post_title || strlen($post_title) < 1) {
  $post_title = get_the_title($mpid);
}
?>
<div class="md-featured" style="background: url('<?php echo $img; ?>') no-repeat center center; background-size: cover;">
  <a href="<?php echo get_the_permalink($mpid); ?>">
    <div class="feature-meta">
      <h3><?php echo $post_title; ?></h3>
      <div class="feature-meta-line">
        <span class="meta-comments"><i class="fa fa-comment-o"></i><?php comments_number( '0', '1', '%' ); ?></span>
        <span class="meta-shares"><?php Healthbeat_Helper::icongram_icon('clarity', 'share', 24, '5489c4'); ?><?php echo do_shortcode('[share_count url="' . get_the_permalink($mpid) . '" id=' . $mpid . ']'); ?></span>
      </div>
    </div>
    <div class="image-gradient"></div>
  </a>
</div>
