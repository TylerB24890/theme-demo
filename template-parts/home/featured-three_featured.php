<div class="three-up">
	<?php
	if(have_rows('large_feature_threeup')) {
		while(have_rows('large_feature_threeup')) {
			the_row();
			include(locate_template(Healthbeat_Helper::$parts . 'home/featured-templates/large-featured.php'));
		}
	}
	?>

	<div class="col-md-5 no-padding">
		<?php
		if(have_rows('medium_features_threeup')) {
			while(have_rows('medium_features_threeup')) {
				the_row();
				include(locate_template(Healthbeat_Helper::$parts . 'home/featured-templates/medium-featured.php'));
			}
		}
		?>
	</div>
</div>
