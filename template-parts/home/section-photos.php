<?php if(have_rows('photos')) : ?>
  <section id="photos" class="large">
    <div class="container-fluid">
      <div class="col-md-12">
        <div class="section-header">
          <h2 class="section-title"><?php _e('Photos', 'healthbeat'); ?></h2>
          <a href="/photo-galleries/" class="btn-outline"><?php _e('View all photo galleries', 'healthbeat'); ?></a>
        </div>
      </div>
    </div>

    <div id="gallery-carousel">
      <?php
        $galleries = array();
        $single_gallery = array();

        while(have_rows('photos')) {
          the_row();

          $single_gallery['photo'] = get_sub_field('photo');
          $single_gallery['gallery'] = get_sub_field('photo_gallery');
          $single_gallery['caption'] = get_sub_field('photo_gallery_caption');

          if(!$single_gallery['caption'] || strlen($single_gallery['caption']) < 1) {
            $gallery_img_id = Healthbeat_Helper::get_attachment_id_by_url($single_gallery['photo']);
            $single_gallery['caption'] = wp_get_attachment_caption($gallery_img_id);
          }

          array_push($galleries, $single_gallery);
        }

        $gc = 0;

        foreach($galleries as $gallery) :
          $gc++;
          $post = $gallery['gallery'];

          setup_postdata($post);

          $gallery_title = get_the_title();
          $gallery_link = get_the_permalink();
          $gallery_photo = $gallery['photo'];
          $gallery_photo_alt = sanitize_title_with_dashes($gallery_title);
          $gallery_caption = $gallery['caption'];
      ?>

            <div class="gallery-item">
              <div class="gallery-photo">
                <img src="<?php echo $gallery_photo; ?>" alt="<?php echo $gallery_title; ?>" title="<?php echo $gallery_photo_alt; ?>" data-url="<?php echo $gallery_link; ?>" />
              </div>
              <div class="gallery-excerpt">
              <h1><?php echo $gallery_title; ?></h1>
              <p><?php echo $gallery_caption; ?></p>
              <a href="<?php echo $gallery_link; ?>" class="btn-outline"><?php _e('Explore this story', 'healthbeat'); ?></a>
              </div>
            </div>
      <?php endforeach; ?>
    </div>
  </section>
<?php endif; wp_reset_postdata(); ?>
