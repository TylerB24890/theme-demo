<div class="four-up">
	<?php
	if(have_rows('large_feature_fourup')) {
		while(have_rows('large_feature_fourup')) {
			the_row();
			include(locate_template(Healthbeat_Helper::$parts . 'home/featured-templates/large-featured.php'));
		}
	}
	?>

	<div class="col-sm-5 no-padding">
		<?php
		if(have_rows('medium_feature_fourup')) {
			while(have_rows('medium_feature_fourup')) {
				the_row();
				include(locate_template(Healthbeat_Helper::$parts . 'home/featured-templates/medium-featured.php'));
			}
		}

		if(have_rows('small_features_fourup')) {
			$spc = 0;
			while(have_rows('small_features_fourup')) {
				the_row();
				$spc++;
				include(locate_template(Healthbeat_Helper::$parts . 'home/featured-templates/small-featured.php'));
			}
		}
		?>
	</div>
</div>
