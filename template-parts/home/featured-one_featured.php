<div class="one-up">
	<?php
	if(have_rows('large_feature_oneup')) {
		while(have_rows('large_feature_oneup')) {
			the_row();

			include(locate_template(Healthbeat_Helper::$parts . 'home/featured-templates/large-featured.php'));
		}
	}
	?>
</div>
