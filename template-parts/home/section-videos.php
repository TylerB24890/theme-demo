<?php if(have_rows('videos')) : ?>
  <section id="videos" class="light-gray large">
    <div class="container-fluid">
      <div class="col-md-12">
        <div class="section-header">
          <h2 class="section-title"><?php _e('Videos', 'healthbeat'); ?></h2>
          <a href="/videos/" class="btn-outline"><?php _e('View all videos', 'healthbeat'); ?></a>
        </div>

        <?php
          $videos = array();

          while(have_rows('videos')) {
            the_row();
            $video = get_sub_field('video');
            array_push($videos, $video);
          }

          $vc = 0;
          foreach($videos as $pid) :
            $vc++;

            if($vc === 1) :
              $video = Healthbeat_Videos::get_video_data($pid);
              $title = get_the_title($pid);
            ?>
            <div class="col-sm-8 no-left-padding">
              <div class="featured-video">
                <div class="video-block">
                  <?php echo $video['embed']; ?>

                  <div class="video-content">

                    <h3><a href="<?php echo $video['url']; ?>"><?php echo $title; ?></a></h3>
                    <div class="video-excerpt">
                      <span class="excerpt"><?php echo get_the_excerpt($pid); ?></span>

                      <!--<a class="video-post-link" href="<?php //echo $video['url']; ?>">View Story <?php //Healthbeat_Helper::icongram_icon('feather', 'arrow-right', 25, '3828f'); ?></a>-->
                    </div>

                    <div class="video-meta">
                      <ul>
                        <li class="video-views">
                          <i class="fa fa-eye"></i> <span><?php echo do_shortcode('[hb_video_views id=' . $video['id'] . ']'); ?></span>
                        </li>
                        <li class="video-shares">
                          <?php Healthbeat_Helper::icongram_icon('clarity', 'share', 22, '497ebb'); ?>
                          <?php do_shortcode('[share_count_return url="' . $video['url'] . '" id="' . $pid . '"]'); ?>
                          <span id="<?php echo $pid; ?>" class="shares"></span>
                        </li>
                        <li class="video-date">
                          <?php echo $video['date']; ?>
                        </li>
                      </ul>

                    </div>
                  </div>
                </div>
              </div>
            </div>
            <?php
            endif;
          endforeach;
          ?>
            <div class="col-sm-4 no-right-padding">
              <div class="video-list">
                <?php
                  $vc = 0;
                  foreach($videos as $pid) :
                    $vc++;

                    $video = Healthbeat_Videos::get_video_data($pid);
                ?>

                  <div class="video-block media<?php echo($vc === 1 ? ' active' : ''); ?>">
                    <div class="media-left">

                      <a href="#"
                        id="<?php echo $pid; ?>"
                        class="video-list-item video-img"
                        data-category-title="<?php echo $video['category']['name']; ?>"
                        data-category-slug="<?php echo $video['category']['slug']; ?>"
                        data-category-url="<?php echo $video['category']['url']; ?>"
                        data-title="<?php echo $video['title']; ?>"
                        data-excerpt="<?php echo $video['excerpt']; ?>"
                        data-views="<?php echo $video['views']; ?>"
                        data-post-url="<?php echo $video['url']; ?>"
                        data-date="<?php echo $video['date']; ?>"
                        data-vid="<?php echo $video['id']; ?>"
                      >

                        <img src="<?php echo $video['thumb']; ?>" class="center-image" alt="<?php echo $video['title']; ?>" title="<?php echo sanitize_title_with_dashes($video['title']); ?>" />

                      </a>
                    </div>
                    <div class="media-body">
                      <div class="active-video-flag">
                        <?php _x('Now playing', 'healthbeat', 'Now Playing video playlist'); ?>
                      </div>
                      <a href="#"
                        class="video-list-item"
                        data-category-title="<?php echo $video['category']['name']; ?>"
                        data-category-slug="<?php echo $video['category']['slug']; ?>"
                        data-category-url="<?php echo $video['category']['url']; ?>"
                        data-title="<?php echo $video['title']; ?>"
                        data-excerpt="<?php echo $video['excerpt']; ?>"
                        data-views="<?php echo $video['views']; ?>"
                        data-shares="<?php echo number_format(intval($video['shares'])); ?>"
                        data-post-url="<?php echo $video['url']; ?>"
                        data-date="<?php echo $video['date']; ?>"
                        data-vid="<?php echo $video['id']; ?>"
                      >
                        <span><?php echo $video['title']; ?></span>
                      </a>
                    </div>
                  </div>

              <?php endforeach; ?>
            </div>
        </div>
      </div>
    </div>
  </section>
<?php wp_reset_postdata(); endif; ?>
