<div class="posts">
  <?php
    wp_reset_query();
    $paged = (get_query_var('page')) ? get_query_var('page') : 1;

    $args = array(
      'paged' => $paged,
      'post_type' => array('post', 'infographics', 'videos', 'patient-stories'),
      'posts_per_page' => 9,
      'post__not_in' => $exclude,
      'cat' => "-98, -3115",
      'tax_query' => array(
        array(
          'taxonomy' => 'expert_blog',
          'field' => 'id',
          'terms' => array(19,20,21,22,23,24,25,26,97),
          'operator' => 'NOT IN',
        )
      )
    );

    $temp = $wp_query;
    $wp_query = null;
    $wp_query = new WP_Query($args);

    if($wp_query->have_posts()) {
      while($wp_query->have_posts()) {
        $wp_query->the_post();
        $exclude[] = $post->ID;
        include(locate_template(Healthbeat_Helper::$parts . 'components/post-list.php'));
      }

      echo '<div class="text-center"><a class="btn btn-default btn-green-gradient large" href="/stories/">' . __('View More Stories', 'healthbeat') . '</a></div>';
    }

    $wp_query = null;
    $wp_query = $temp;

    wp_reset_query();
  ?>
</div>
