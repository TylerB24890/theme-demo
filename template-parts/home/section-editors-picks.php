<?php if(have_rows('editors_picks')) : ?>
<section id="editors-picks">
  <div class="container-fluid">
    <div class="col-md-12">
      <div class="section-header">
        <h2 class="section-title"><?php _e('Editor\'s Picks', 'healthbeat'); ?></h2>
      </div>
    </div>
    <?php

      $class = 'col-md-3';

      while(have_rows('editors_picks')) {
        the_row();

        $post = get_sub_field('post');
        setup_postdata($post);
        include(locate_template(Healthbeat_Helper::$parts . 'components/post-block.php'));
        wp_reset_postdata();
      }

    ?>
  </div>
</section>
<?php endif; ?>
