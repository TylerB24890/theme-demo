<section id="homepage-subscribe">
  <div class="container-fluid">
    <div class="subscribe-container">
      <div class="col-lg-7 col-md-9 center-block">
        <div class="subscribe-box light-gray">
          <?php Healthbeat_Helper::render_svg_img('subscribe-icon', 93, 85); ?>

          <div class="subscribe-title">
            <h2 class="section-title"><?php _e('Free e-News', 'healthbeat'); ?></h2>
            <p class="section-intro"><?php _e('Our best stories delivered to your inbox!', 'subscribe'); ?></p>
          </div>

          <a href="<?php echo Healthbeat_Helper::$subscribe_url; ?>" class="btn btn-default btn-green-gradient large"><?php _e('Subscribe', 'healthbeat'); ?></a>
        </div>
      </div>
    </div>
  </div>
</section>
