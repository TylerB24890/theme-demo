<?php
  $spotlight = false;
  if(have_rows('category_spotlights', $cat_selector)) {
    $spotlight = true;
  }
?>

<div class="category-header <?php echo $cat_slug; ?><?php echo($spotlight ? '' : ' no-spotlight'); ?>">
  <div class="<?php echo($spotlight ? 'col-md-4' : 'col-md-12'); ?> no-padding">
    <div class="category-flag <?php echo $cat_slug; ?>">
      <?php echo $cat_name; ?>
    </div>
    <?php if($parent_cat_id !== 0) : ?>
      <div class="category-flag-subcat <?php echo $cat_slug . '-text'; ?>">
       <?php echo $child_name; ?>
      </div>
    <?php endif; ?>
    <div class="category-description">
      <?php echo category_description(); ?>
    </div>
  </div>
  <?php
    if($spotlight) :
  ?>
    <div class="col-md-8 double-left-padding no-right-padding">
    <?php
      while(have_rows('category_spotlights', $cat_selector)) :
        the_row();

        $spotlight_post = get_sub_field('spotlight_post', $cat_selector);

        $post_data = Healthbeat_Posts::get_post_data($spotlight_post);

        $exclude[] = $post_data['id'];

        $spotlight_title = get_sub_field('spotlight_title', $cat_selector);
        if(!$spotlight_title || strlen($spotlight_title) < 1) {
          $spotlight_title = $post_data['title'];
        }

        $spotlight_excerpt = get_sub_field('spotlight_excerpt', $cat_selector);
        if(!$spotlight_excerpt || strlen($spotlight_excerpt) < 1) {
          $spotlight_excerpt = $post_data['excerpt'];
        }

        $spotlight_img = get_sub_field('spotlight_image', $cat_selector);
        if(!$spotlight_img || strlen($spotlight_img) < 1) {
          $spotlight_img = get_the_post_thumbnail_url($post_data['id']);
        }
    ?>
        <div id="featured-posts" class="category-spotlight">
          <div class="lg-featured" style="background: url('<?php echo $spotlight_img; ?>') no-repeat center center; background-size: cover;">
            <a href="<?php echo $post_data['url']; ?>">
              <div class="feature-meta">
                <h2><?php echo $spotlight_title; ?></h2>
                <p>
                  <?php echo $spotlight_excerpt; ?>
                </p>
                <div class="feature-meta-line">
                  <!--<span class="date"><?php echo $post_data['date']; ?></span>-->

                  <div class="author">
                    <span class="author-img"><?php echo $post_data['author']['image']; ?></span>
                    <span class="author-name"><?php _x('By', 'healthbeat', 'Author name byline'); ?> <?php echo $post_data['author']['name']; ?></span>
                  </div>

                  <span class="meta-comments"><i class="fa fa-comment-o"></i><?php comments_number( '0', '1', '%' ); ?></span>
                  <?php

                  ?>
                  <span class="meta-shares"><?php Healthbeat_Helper::icongram_icon('clarity', 'share', 24, '5489c4'); ?><?php echo do_shortcode('[share_count url="' . $post_data['url'] . '" id="' . $post_data['id'] . '"]'); ?></span>
                </div>
              </div>
              <div class="image-gradient"></div>
            </a>
          </div>
        </div>
    <?php endwhile; ?>
    </div>
  <?php wp_reset_query(); endif; ?>
</div>
