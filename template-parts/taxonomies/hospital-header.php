<div class="top-section">
  <div class="hospital-header">
    <div id="featured-posts">
      <div class="hospital-image lg-featured" style="background: url('<?php the_field('hospital_image', $tax); ?>') no-repeat center center; backgroud-size: cover !important;">
        <div class="col-sm-9">
          <h2><?php echo $tax->name; ?></h2>
        </div>
        <div class="col-sm-3 no-padding">
          <div class="feature-meta">

            <ul class="hospital-info">
              <?php if(get_field('hospital_name', $tax)) : ?>
                <li>
                  <h3><?php the_field('hospital_name', $tax); ?></h3>
                </li>
              <?php endif; ?>
              <?php if(get_field('hospital_address', $tax)) : ?>
                <li>
                  <a href="https://maps.google.com/?q=<?php echo urlencode(get_field('hospital_address', $tax)); ?>">
                    <i class="fa fa-map-marker"></i> <?php the_field('hospital_address', $tax); ?>
                  </a>
                </li>
              <?php endif; ?>

              <?php
              $phone = get_field('hospital_phone', $tax);

              if($phone) :
                $phone_link = preg_replace('/[^0-9]/', '', $phone);
              ?>
                <li>
                  <a href="tel:<?php echo $phone_link; ?>">
                    <i class="fa fa-phone"></i> <?php echo $phone; ?>
                  </a>
                </li>

              <?php endif; ?>

              <?php if(get_field('hospital_url', $tax)) : ?>
                <li>
                  <a href="<?php echo get_field('hospital_url', $tax); ?>">
                    <i class="fa fa-globe"></i> <?php echo str_replace('://', '', strstr(get_field('hospital_url', $tax), '://')); ?>
                  </a>
                </li>
              <?php endif; ?>
              <?php if(get_field('hospital_social_url', $tax)) : ?>
                <li>
                  <a href="<?php echo get_field('hospital_social_url', $tax); ?>">
                    <i class="fa fa-<?php echo get_field('social_url_type', $tax); ?>"></i> <?php echo '/' . basename(get_field('hospital_social_url', $tax));?>
                  </a>
                </li>
              <?php endif; ?>

            </ul>

            <?php if(strlen(get_field('hospital_name2', $tax)) > 1) : ?>
              <ul class="hospital-info">
                <li>
                  <h3><?php the_field('hospital_name2', $tax); ?></h3>
                </li>
                <?php if(get_field('hospital_address2', $tax)) : ?>
                  <li>
                    <a href="https://maps.google.com/?q=<?php echo urlencode(get_field('hospital_address2', $tax)); ?>">
                      <i class="fa fa-map-marker"></i> <?php the_field('hospital_address2', $tax); ?>
                    </a>
                  </li>
                <?php endif; ?>

                <?php
                $phone = get_field('hospital_phone2', $tax);

                if($phone) :
                  $phone_link = preg_replace('/[^0-9]/', '', $phone);
                ?>
                  <li>
                    <a href="tel:<?php echo $phone_link; ?>">
                      <i class="fa fa-phone"></i> <?php echo $phone; ?>
                    </a>
                  </li>

                <?php endif; ?>

                <?php if(get_field('hospital_url2', $tax)) : ?>
                  <li>
                    <a href="<?php echo get_field('hospital_url2', $tax); ?>">
                      <i class="fa fa-globe"></i> <?php echo str_replace('://', '', strstr(get_field('hospital_url2', $tax), '://')); ?>
                    </a>
                  </li>
                <?php endif; ?>
                <?php if(get_field('hospital_social_url2', $tax)) : ?>
                  <li>
                    <a href="<?php echo get_field('hospital_social_url2', $tax); ?>">
                      <i class="fa fa-<?php echo get_field('social_url_type2', $tax); ?>"></i> <?php echo '/' . basename(get_field('hospital_social_url2', $tax));?>
                    </a>
                  </li>
                <?php endif; ?>
              </ul>
            <?php endif; ?>
          </div>
        </div>
        <div class="image-gradient"></div>
      </div>
    </div>

    <div class="hospital-description">
      <?php echo term_description($tax->term_id, 'hospitals'); ?>
    </div>

    <h3><?php _x('Posts for', 'healthbeat', 'Posts for X hospital'); ?> <?php echo $tax->name; ?></h3>
  </div>
</div>
