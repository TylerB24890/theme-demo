<?php
/**
 * healthbeat functions and definitions.
 *
 * @package healthbeat
 */

 $whitelist = array(
     '127.0.0.1',
     '::1'
 );

if(in_array($_SERVER['REMOTE_ADDR'], $whitelist)){
    define('HEALTHBEAT_DEV', true);
} else {
	define('HEALTHBEAT_DEV', false);
}

if ( ! function_exists( 'healthbeat_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function healthbeat_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on healthbeat, use a find and replace
	 * to change 'healthbeat' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'healthbeat', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'healthbeat'),
		'secondary' => esc_html__( 'Secondary', 'healthbeat'),
		'footer' => esc_html__( 'Footer', 'healthbeat')
	) );

  add_image_size('post-block', 320, 140, array('center', 'center'));
	add_image_size('post-list', 385, 185, array('center', 'center'));
	add_image_size('lg-featured-main', 800, 590, array('center', 'center'));
	add_image_size('md-featured-4', 650, 335, array('center', 'center'));
	add_image_size('sm-featured-4', 330, 225, array('center', 'center'));
  add_image_size('sidebar-block', 320, 260, array('center', 'center'));
  add_image_size('infographic-carousel', 360, 530);
  add_image_size('infographic-block', 380, 9999, false);

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

    remove_filter( 'the_excerpt', 'wpautop' );

	// Remove unnecessary elements from header
	remove_action('wp_head', 'rsd_link');
	remove_action('wp_head', 'wp_generator');
	remove_action('wp_head', 'feed_links', 2);
	remove_action('wp_head', 'index_rel_link');
	remove_action('wp_head', 'wlwmanifest_link');
	remove_action('wp_head', 'feed_links_extra', 3);
	remove_action('wp_head', 'start_post_rel_link', 10, 0);
	remove_action('wp_head', 'parent_post_rel_link', 10, 0);
	remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0);
}
endif;
add_action( 'after_setup_theme', 'healthbeat_setup' );

// Register the nav menu locations
register_nav_menus( array(
	'primary' => esc_html__( 'Primary', 'healthbeat'),
	'secondary' => esc_html__( 'Secondary', 'healthbeat'),
	'footer' => esc_html__( 'Footer', 'healthbeat')
) );

// ACF Widget Titles
add_filter('show_acfw_titles', '__return_true');

// Include ACF
include_once(get_template_directory() . '/inc/acf-include.php');

// Include Health Beat core functions
include_once(get_template_directory() . '/inc/healthbeat/healthbeat.php');

// Include theme specific functions
include_once(get_template_directory() . '/inc/theme-functions.php');

// Include Popular Posts customization
include_once(get_template_directory() . '/inc/healthbeat-popular-posts.php');

// Customizer
include_once(get_template_directory() . '/inc/class-healthbeat-customizer.php');

// Enqueue scripts
include_once(get_template_directory() . '/inc/enqueue-scripts.php');

// Nav Walker
include_once(get_template_directory() . '/inc/class-healthbeat-nav-walker.php');

// Load Pagination file
include_once(get_template_directory() . '/inc/pagination.php');
