<?php
/**
 * Individual Special Series Landing Page
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package healthbeat
 */

get_header();

$series_slug = get_query_var('term');

$series_data = get_term_by('slug', $series_slug, 'special-series');
$series_id = $series_data->term_id;

$series_selector = "special-series_" . $series_id;
$series_image = get_field('series_image', $series_selector);
$series_html_title = get_field('series_html_title', $series_selector);
?>

<div id="series-landing" class="container-fluid page">
	<div class="col-sm-9 no-right-padding">

		<div class="col-md-12">
			<div class="page-header-container series-header">
        <div class="col-md-6 no-padding">
					<div class="series-flag">
						<?php echo sprintf('A <span class="health">%s</span><span class="beat">%s</span> Special Series', __('Health', 'healthbeat'), __('Beat', 'healthbeat')); ?>
					</div>
          <h1><?php echo strip_tags($series_html_title); ?></h1>
          <p>
            <?php echo term_description($series_id, 'special-series'); ?>
          </p>
        </div>

				<div class="col-md-6 text-right no-padding">
					<img src="<?php echo $series_image; ?>" alt="<?php _e('Health Beat Special Series', 'healthbeat'); ?>" title="<?php echo $series_slug; ?>">
				</div>
			</div>
		</div>

      <div class="clear clearfix"></div>
      <?php if(have_posts()) : ?>
        <div class="posts">
          <?php while(have_posts()) : the_post(); ?>
            <?php include(locate_template(Healthbeat_Helper::$parts . 'components/post-list.php')); ?>
          <?php endwhile; ?>

					<?php healthbeat_pagination(); ?>
        </div>
      <?php endif; ?>
	</div>
	<div class="col-sm-3">
		<?php get_sidebar('main-sidebar'); ?>
	</div>
</div><!-- .container -->

<?php
get_footer();
