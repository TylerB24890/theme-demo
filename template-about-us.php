<?php
/**
 * Template Name: About Us
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package healthbeat
 */

get_header();

?>

<div id="about-us" class="container-fluid page">
	<div class="col-sm-9 no-right-padding">

    <?php
      if(have_posts()) :
        while(have_posts()) :
          the_post();
    ?>
      		<div class="col-md-12">
      			<div class="page-header-container">
      				<h1><?php the_title(); ?></h1>

              <div class="col-md-8 no-left-padding">
                <?php the_content(); ?>
              </div>
              <div class="col-md-4 no-right-padding">
                <?php
                  /** Managing Editor **/
                  $author = get_user_by('slug', 'cwelch');
                  include_once(locate_template(Healthbeat_Helper::$parts . 'components/author-block.php'));
                ?>
              </div>
      			</div>
      		</div>

          <div class="clear clearfix"></div>

          <div class="author-list">
            <?php
              // Get the top/static authors
              $top_authors = array(43, 78, 10000005, 10000012);

              // Top Author Args
              $top_author_args = array(
                'include' => $top_authors,
                'orderby' => 'ID'
              );

              // Add managing editor ID to top authors array
              $top_authors[] = $author->ID;

              // Get top authors
              Healthbeat_Authors::author_loop($top_author_args);

              // Create the contributor author query
              $contributor_args = array(
                'exclude' => $top_authors,
                'role' => 'contributor'
              );

              Healthbeat_Authors::author_loop($contributor_args);

              // Create the blogger author query
              $blogger_args = array(
                'exclude' => $top_authors,
                'role' => 'blogger'
              );

              Healthbeat_Authors::author_loop($blogger_args);
            ?>
          </div>

    <?php endwhile; endif; ?>
	</div>
	<div class="col-sm-3">
		<?php get_sidebar('main-sidebar'); ?>
	</div>
</div><!-- .container -->

<?php
get_footer();
