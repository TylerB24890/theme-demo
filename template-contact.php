<?php
/**
 * Template Name: Contact Us
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package healthbeat
 */

get_header();

if(have_posts()) :
	while(have_posts()) :
		the_post();
?>

		<div id="contact-us" class="container-fluid page">
			<div class="col-sm-9 no-right-padding">
				<div class="col-md-12">
					<div class="page-header-container">
						<div class="col-md-3 no-left-padding">

							<?php
							$cta_args = array(
								'type' => 'cta-stacked',
								'icon' => array(
									'name' => 'share-your-story-icon',
									'width' => 116,
									'height' => 105
								),
								'title' => __('Share your story', 'healthbeat'),
								'content' => __('Do you have a story you\'d like to share about your experience as a patient at Spectrum Health?', 'healthbeat'),
								'button' => array(
									'url' => '/contact-us/share-your-story/',
									'text' => __('Get started', 'healthbeat')
								)
							);

							Healthbeat_Helper::render_cta($cta_args);
							?>
						</div>
						<div class="col-md-9 no-right-padding">
							<h1><?php the_title(); ?></h1>

							<p>
								<?php echo Healthbeat_Helper::make_url_link(get_field('page_description')); ?>
							</p>

							<div class="page-content">
								<?php the_content(); ?>
							</div>
						</div>
					</div>
				</div>
				<div class="clear clearfix"></div>
			</div>

			<div class="col-sm-3">
				<?php get_sidebar('main-sidebar'); ?>
			</div>
		</div><!-- .container -->

<?php
	endwhile;
endif;
get_footer();
