<?php

/* Category Archive */

get_header();

$tax = get_queried_object();
?>

<div class="container-fluid">
  <div class="top-section">
    <div class="col-sm-9 no-right-padding">


			<?php include(locate_template(Healthbeat_Helper::$parts . 'taxonomies/hospital-header.php')); ?>


      <div class="posts">
        <?php

          if(have_posts()) {
            while(have_posts()) {
              the_post();
              include(locate_template(Healthbeat_Helper::$parts . 'components/post-list.php'));
            }

            healthbeat_pagination();
          }

          wp_reset_postdata();
        ?>
      </div>
    </div>

    <div class="col-sm-3">
      <?php get_sidebar('main-sidebar'); ?>
    </div>
  </div>

  <div class="clear clearfix"></div>
</div>
<?php get_footer(); ?>
