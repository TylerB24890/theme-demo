/**
 * Gruntfile.js Configuration
 * Change out the variables below with your site variables
 */

module.exports = {
    siteUrl:       'healthbeat.local',
    sassDir:          './scss/',
    sassMainFileName: 'style',
    cssDir:           './styles/',
    cssMainFileDir:   './styles/',
    cssMainFileName:  'style',
    jsDir:            './js/',
    imgDir:           './img/',
    imgSourceDir:     'img/',
};
