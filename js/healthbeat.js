/**
* healthbeat theme main JS file
* @author   Elexicon, Inc.
*
* Available Variables:
*
* healthbeat.ajaxurl = (string) wp-admin AJAX url
* healthbeat.isMobile = (bool) if the user agent is a mobile device
* healthbeat.isHome = (bool) if the user is on the homepage
* healthbeat.curPage = (string) slug of the current page
* healthbeat.isSingle = (bool) if the user is on a single post
*/

(function($) {

    'use strict';

    var $window = $(window);
    var ww = $window.width();

    // URL Variables
    var pathArray = window.location.href.split( '/' );
    var protocol = pathArray[0];
    var host = pathArray[2];
    var directory = pathArray[3];
    var domainArray = host.split(".");
    var baseDomain = "";
    var domainExt = "";
    if(domainArray.length > 2) {
      baseDomain = domainArray[1];
      domainExt = domainArray[2];
    } else {
      baseDomain = domainArray[0];
      domainExt = domainArray[1];
    }
    var baseURL = "." + baseDomain + "." + domainExt;
    var fullURL = protocol + '//' + host;

    // Is touch device?
    var isTouch = !!("undefined" != typeof document.documentElement.ontouchstart)

    /**
     * Add 'target="_blank"' to all external links
     * @return string target=_blank attribute
     */
    addLinkTarget(baseURL);

    /**
     * Animate the Bootstrap hamburger to an X on click
     */
		$('.navmenu').on('show.bs.offcanvas', function() {
			$('.backdrop').addClass('coverDark');
			$('.navbar-toggle').addClass('active');
		});
		$('.navmenu').on('hide.bs.offcanvas', function() {
			$('.backdrop').removeClass('coverDark');
			$('.navbar-toggle').removeClass('active');
		});

    /**
     * Inline scrolling
     * @param  event e capture the JS click event
     * @return false - prevents link redirection
     */
    $('a.inline-link').click(function(e) {
				inlineLinkScroll($(this).attr('href'), 1200);
        return false;
    });

    // If sidebar widget area is empty, remove it from the DOM
    $('#sidebar .widget-area').each(function(i) {
      if($(this).find('.widget-title').length < 1 && $(this).find('div').length < 1) {
        $(this).remove();
      }
    });

		// Remove Uncategorized Category
		$('li.cat-item > a').each(function() {
			if($(this).text() == 'Uncategorized') {
				$(this).parents('li.cat-item').remove();
			}
		});

		// Highlight Popular Posts on Hover
    $('.popular-post-image').mouseenter(function() {
      $(this).parents('.popular-post').find('h3 > a').addClass('hovered');
    });
    $('.popular-post-image').mouseleave(function() {
      $(this).parents('.popular-post').find('h3 > a').removeClass('hovered');
    });

    // Open Share Box on Click of Share Link
    $('ul.share-list li > a, .share-link').on('click', function(e) {
    	e.preventDefault();
			openShareBox($(this).attr('href'));
    	return false;
    });

    /**
     * Homepage Scripts
     */
		if(healthbeat.isHome || healthbeat.curPage === 'home') {

			// Video Playlist
			var videoContainer = $('#videos .featured-video > .video-block');
	    $('.video-list .video-block a.video-list-item').on('click', function(e) {
	      e.preventDefault();

	      $('.video-block').removeClass('active');
	      $(this).parents('.video-block').addClass('active');

	      var pid = $(this).attr('id');
	      var vid = $(this).data('vid');
	      var postURL = $(this).data('post-url');
	      var title = $(this).data('title');
	      var excerpt = $(this).data('excerpt');
	      var views = $(this).data('views');
	      var categoryName = $(this).data('category-title');
	      var categorySlug = $(this).data('category-slug');
	      var categoryURL = $(this).data('category-url');
	      var date = $(this).data('date');
	      //var shares = $(this).data('shares');

	      var iFrameURL = 'https://www.youtube.com/embed/' + vid + '?feature=oembed';

	      // Change Video URL
	      videoContainer.find('iframe').attr('src', iFrameURL);
	      // Change Category Flag
	      videoContainer.find('.video-content > a.category-flag').removeClass().addClass('category-flag ' + categorySlug).attr('href', categoryURL).text(categoryName);
	      // Change video title
	      videoContainer.find('.video-content > h3 > a').text(title).attr('href', postURL);
	      // Change video excerpt
	      videoContainer.find('.video-content > .video-excerpt > span.excerpt').text(excerpt);
	      // Change video post link
	      videoContainer.find('.video-content > .video-excerpt > a.video-post-link').attr('href', postURL);
	      // Change View count
	      videoContainer.find('.video-content > .video-meta > ul > li.video-views > span').text(views);

	      // Change shares
	      videoContainer.find('.video-content > .video-meta > ul > li.video-shares > span.shares').attr('id', pid);
	      $.get(healthbeat.ajaxurl, {
	        action: "get_all_shares",
	        url: postURL,
	        id: pid
	      }).done(function(data) {
	        var shares = Number(data);
	        if(!isNaN(shares)) {
	          videoContainer.find('.video-content > .video-meta > ul > li.video-shares > span.shares').html(shares);
	        } else {
	          videoContainer.find('.video-content > .video-meta > ul > li.video-shares > span.shares').html("0");
	        }
	      });

	      // Change video date
	      videoContainer.find('.video-content > .video-meta > ul > li.video-date').text(date);
	    });

	    // Infographic Carousel
	    $('#infographic-carousel').slick({
	      infinite: true,
	      slidesToShow: 4,
	      slidesToScroll: 3,
	      swipeToSlide: true,
	      arrows: false,
	      variableWidth: true
	    });

	     // Homepage Photo Gallery Carousel
	    $('#gallery-carousel').on('setPosition', function(event, slick) {
	      $(this).find('.gallery-item').each(function(i) {
	        var imgWidth = $(this).find('img').width();
	        $(this).find('.gallery-excerpt').css('max-width', imgWidth + 'px');
	      });
	    });
	    $('#gallery-carousel').on('afterChange', function(event, slick, currentSlide) {
	      $(this).find('.gallery-item > .gallery-photo > img').on('click', function(e) {
	        if($(this).parents('.gallery-item').hasClass('slick-current')) {
	          window.location.href = $(this).data('url');
	        }
	      });
	    });
	    $('#gallery-carousel').slick({
	      slidesToShow: 3,
	      swipeToSlide: true,
	      slidesToScroll: 1,
	      variableWidth: true,
	      focusOnSelect: true,
				centerMode: false,
	    });
		}

		/**
		 * Single Post Scripts
		 */
		if(healthbeat.isSingle) {
			// Single Photo Gallery
	    $('.post-gallery').slick({
	      slidesToShow: 1,
	      slidesToScroll: 1,
	      arrows: true,
	      fade: false,
	      asNavFor: '.post-gallery-thumbs',
				infinite: false,
	    });
	    $('.post-gallery-thumbs').slick({
	      slidesToShow: 7,
	      slidesToScroll: 1,
	      asNavFor: '.post-gallery',
	      dots: false,
	      arrows: true,
	      centerMode: false,
	      focusOnSelect: true,
	      infinite: false,
	      swipeToSlide: true,
	    });

			if(healthbeat.isMobile || isTouch || ww < 991) {
				$('a[data-fancybox="gallery"]').each(function() {
					$(this).removeAttr('data-fancybox');
					$(this).attr('href', 'javascript:void(0);');
				});
			}
		}

		/*
		 *
		 * Remove the GTM tag information for demo purposes

		// Photo Gallery DataLayer swiping
		$('.post-gallery').on('swipe', function(event, slick, direction) {
			if(direction === 'left') {
				dataLayer.push({'category': 'photo-gallery', 'action' : 'photo-gallery-swipe-next', 'label' : 'next', 'event' : 'photo-click'});
			} else if(direction === 'right') {
				dataLayer.push({'category': 'photo-gallery', 'action' : 'photo-gallery-swipe-previous', 'label' : 'previous', 'event' : 'photo-click'});
			}
		});
		// Photo Gallery Thumbs DataLayer Swiping
		$('.post-gallery-thumbs').on('swipe', function(event, slick, direction) {
			if(direction === 'left') {
				dataLayer.push({'category': 'photo-gallery-thumbs', 'action' : 'thumb-swipe-next', 'label' : 'thumb-next', 'event' : 'photo-click'});
			} else if(direction === 'right') {
				dataLayer.push({'category': 'photo-gallery-thumbs', 'action' : 'thumb-swipe-previous', 'label' : 'thumb-previous', 'event' : 'photo-click'});
			}
		});
		*/

		/**
		 * Infographics Landing Scripts
		 */
		if(healthbeat.curPage === 'infographics') {
			// Infographics Masonry
			$('.infographic-masonry').masonry({
				itemSelector: '.grid-item',
				columnWidth: '.grid-sizer',
				percentPosition: true,
			});
			$('.infographic-masonry').imagesLoaded(function() {
				$('.infographic-masonry').masonry();
			});

			$.fn.almComplete = function(alm) {
				$('.infographic-masonry').masonry('reloadItems');
				$('.infographic-masonry').imagesLoaded(function() {
					$('.infographic-masonry').masonry();
				});
			}
		}

		// Remove Empty Tags
		$('.single-cta p, .single-cta h4').each(function() {
			var $this = $(this);
		  if($this.html().replace(/\s|&nbsp;/g, '').length == 0)
				$this.remove();
		});
})( jQuery );

/**
 * Add 'target="_blank"' to all external links
 * @param	 string URL to match against
 * @return string target=_blank attribute
 */
function addLinkTarget(url) {
	$('a').each(function() {
		$(this).attr('target', (this.href.match( url )) ? '_self' :'_blank');
	});
}

/**
 * Inline scrolling
 * @param  string target to scroll to
 * @param  int 		speed to scroll
 * @return false - prevents link redirection
 */
function inlineLinkScroll(target, speed) {
	$('html, body').animate({
			scrollTop: $(target).offset().top
	}, speed);
}

/**
 * Open Social Share Box
 * @param  string url URL to share
 * @return null
 */
function openShareBox(url) {
	window.open(url, '', 'height=450, width=550, top=' + ($(window).height() / 2 - 275) + ', left=' + ($(window).width() / 2 - 225) + ', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');
}
